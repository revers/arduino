#include <cassert>
#include <cstdio>
#include <ctime>

#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <utility>

#include "serial.h"

#define COM_PORT    "COM8"
#define BAUD_RATE   200000
#define PAGE_SIZE   256
#define PAGE_COUNT  2048

#define EXPECTED_SIZE   (PAGE_SIZE * PAGE_COUNT)

#define STATUS_PREFIX      '\x01'
#define STATUS_PREFIX_STR  "\x01"
#define END_OF_DATA        0x03
#define ESCAPE             0x1B

#define STATUS_MESSAGE   (STATUS_PREFIX_STR ":")
#define STATUS_READY     (STATUS_PREFIX_STR "READY")
#define STATUS_ERROR     (STATUS_PREFIX_STR "ERROR")
#define STATUS_DONE      (STATUS_PREFIX_STR "DONE")
#define STATUS_PROCEED   (STATUS_PREFIX_STR "GO")
#define STATUS_MESSAGE   (STATUS_PREFIX_STR ":")

std::string repr(const std::string& line)
{
	std::ostringstream oss;

	for (char c : line)
	{
		if (c < 32 || c > 126)
		{
			oss << "\\x" << std::uppercase << std::setfill('0') << std::setw(2) << std::hex
					<< ((int) c);
		}
		else
		{
			oss << c;
		}
	}
	return oss.str();
}

void logHost(const std::string& msg)
{
	std::cout << "  [Host] " << msg << std::endl;
}

void logDevice(const std::string& msg)
{
	std::cout << "[Device] ";

	if (msg.find(STATUS_MESSAGE) == 0)
	{
		std::cout << repr(msg.substr(strlen(STATUS_MESSAGE)));
	}
	else
	{
		std::cout << repr(msg);
	}
	std::cout << std::endl;
}

void showProgress(const std::string& msg)
{
	std::cout << "\r         " << msg;
	fflush(stdout);
}

void printPorts(std::vector<serial::PortInfo>& ports)
{
	logHost("Found " + std::to_string(ports.size()) + " ports: ");

	for (const serial::PortInfo& portInfo : ports)
	{
		logHost(
				" - " + portInfo.port + ", Description: " + portInfo.description + ", Hardware ID: "
						+ portInfo.hardware_id);
	}
}

void sendCommand(serial::Serial& serialConnection, const std::string& command, bool printCommand = true)
{
	if (printCommand)
	{
		logHost("Command: " + command);
	}
	serialConnection.write(command + ';');
}

std::string getLine(serial::Serial& serialConnection)
{
	std::string line = "";

	while (true)
	{
		std::string readBytes = serialConnection.read();

		for (char c : readBytes)
		{
			if (c == '\r')
			{
				continue;
			}
			if (c == '\n')
			{
				return line;
			}
			else
			{
				line += c;
			}
		}
	}
	return line;
}

struct BinLine
{
	bool isStatus = false;
	std::vector<uint8_t> data;

	std::string toString()
	{
		data.push_back(0);
		std::string result = (const char*) &data[0];
		data.pop_back();

		return result;
	}
};

BinLine getBinaryLine(serial::Serial& serialConnection)
{
	BinLine result;

	bool doEscape = false;

	while (true)
	{
		char c;
		size_t bytesRead = serialConnection.read((uint8_t*) &c, 1);

		if (bytesRead < 1)
		{
			continue;
		}
		if (!doEscape && c == '\x1B')
		{
			doEscape = true;
			continue;
		}
		if (!doEscape && c == '\n')
		{
			result.data.pop_back(); // because the last element is always useless \r
			return result;
		}
		else
		{
			if (result.data.empty() && !doEscape && c == STATUS_PREFIX)
			{
				// If the very first letter is STATUS_PREFIX then we received a status,
				// not a binary data.
				// Note that in binary data the status character is always escaped.
				result.isStatus = true;
			}

			doEscape = false;
			result.data.push_back((uint8_t) c);
		}
	}
	return BinLine();
}

std::string formatDuration(int64_t c)
{
	std::ostringstream oss;

	oss << std::setw(2)
			<< std::setfill('0')          // set field fill character to '0'
			<< (c % 1000000000) / 1000000 // format seconds
			<< "."
			<< std::setw(3)               // set width of milliseconds field
			<< (c % 1000000) / 1000;       // format milliseconds
	return oss.str();
}

template<typename F, typename ... Args>
bool measureTime(F func, Args&&... args)
{
	using namespace std::chrono;

	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	bool result = func(std::forward<Args>(args)...);
	int64_t elapsed = duration_cast<microseconds>(high_resolution_clock::now() - t1).count();

	logHost("Executed in: " + formatDuration(elapsed));

	return result;
}

void printInHex(const std::vector<uint8_t>& data)
{
	int counter = 0;

	for (uint8_t c : data)
	{
		if (counter > 0 && (counter % 16) == 0)
		{
			std::cout << '\n';
		}
		counter++;
		std::cout << std::setfill('0') << std::setw(2) << std::hex << (int) c;
	}
	std::cout << std::endl;
}

bool executeReadPageCommand(serial::Serial& serialConnection, int pageNum)
{
	sendCommand(serialConnection, "read_page:" + std::to_string(pageNum));

	while (true)
	{
		BinLine binLine = getBinaryLine(serialConnection);

		if (binLine.isStatus)
		{
			std::string line = binLine.toString();

			if (line == STATUS_DONE)
			{
				logHost("Done");
				return true;

			}
			else if (line == STATUS_ERROR)
			{
				logHost("Error");
				return true;
			}
			else
			{
				logDevice(line);
			}
		}
		else
		{
			logHost("Response length: " + std::to_string(binLine.data.size()));
			printInHex(binLine.data);
		}
	}
	return false;
}

void saveHexData(const std::string& fileName, const std::vector<uint8_t>& data)
{
	std::fstream file(fileName, std::ios::out);

	for (int i = 0; i < (int) data.size(); i++)
	{
		if (i != 0 && (i % PAGE_SIZE) == 0)
		{
			file << '\n';
		}
		file << std::setfill('0') << std::setw(2) << std::hex << (int) data[i];
	}
	file << '\n';
}

void saveBinData(const std::string& fileName, const std::vector<uint8_t>& data)
{
	std::fstream file(fileName, std::ios::out | std::ios::binary);
	file.write((const char*) &data[0], data.size());
}

bool executeReadAllCommand(serial::Serial& serialConnection)
{
	sendCommand(serialConnection, "read_all");
	std::vector<uint8_t> data;

	while (true)
	{
		BinLine binLine = getBinaryLine(serialConnection);

		if (binLine.isStatus)
		{
			std::string line = binLine.toString();

			if (line == STATUS_DONE)
			{
				std::cout << std::endl;

				if (data.size() == 0)
				{
					logHost("Error: Received 0 bytes.");
					return false;
				}
				std::string outputTxtFileName = "output.txt";
				std::string outputBinFileName = "output.bin";

				logHost("Saving results to " + outputTxtFileName + ", " + outputBinFileName);

				saveBinData(outputBinFileName, data);
				saveHexData(outputTxtFileName, data);

				logHost("Files " + outputTxtFileName + ", " + outputBinFileName + " have been saved.");

				if (data.size() != EXPECTED_SIZE)
				{
					logHost(
							"Error: Received bytes: " + std::to_string(data.size()) + ". Expected: "
									+ std::to_string(EXPECTED_SIZE));
					return false;
				}
				return true;
			}
			else if (line == STATUS_ERROR)
			{
				logHost("Error");
				return true;
			}
			else
			{
				logDevice(line);
			}
		}
		else
		{
			data.insert(data.end(), binLine.data.begin(), binLine.data.end());
			double prc = 100.0 * data.size() / EXPECTED_SIZE;

			std::ostringstream oss;
			oss << std::fixed << std::setprecision(1) << prc << "% (" << data.size() << " bytes)";

			showProgress(oss.str());
		}
	}
	return false;
}

bool executeSimpleCommand(serial::Serial& serialConnection, const std::string& command)
{
	sendCommand(serialConnection, command);

	while (true)
	{
		std::string line = getLine(serialConnection);

		if (line == STATUS_DONE)
		{
			logHost("Done");
			return true;
		}
		else if (line == STATUS_ERROR)
		{
			logHost("Error");
			return false;
		}
		else
		{
			logDevice(line);
		}
	}
	return false;
}

bool executeEraseAllCommand(serial::Serial& serialConnection)
{
	return executeSimpleCommand(serialConnection, "erase_all");
}

bool executeEraseSectorCommand(serial::Serial& serialConnection, int sectorNum)
{
	return executeSimpleCommand(serialConnection, "erase_sector:" + std::to_string(sectorNum));
}

bool writePage(serial::Serial& serialConnection, int pageNum, uint8_t* data, int dataSize)
{
	if (dataSize != PAGE_SIZE)
	{
		logHost(
				"Error: Write Page: Data size is incorrect: " + std::to_string(dataSize) + ". Expected: "
						+ std::to_string(PAGE_SIZE));
		return false;
	}
	std::vector<uint8_t> escapedData;

	for (int i = 0; i < dataSize; i++)
	{
		uint8_t c = data[i];

		if (c == (uint8_t) END_OF_DATA || c == (uint8_t) ESCAPE)
		{
			escapedData.push_back(ESCAPE);
		}
		escapedData.push_back(c);
	}
	escapedData.push_back(END_OF_DATA);

	sendCommand(serialConnection, "write_page:" + std::to_string(pageNum), false);

	while (true)
	{
		std::string line = getLine(serialConnection);

		if (line == STATUS_PROCEED)
		{
			serialConnection.write(escapedData);
		}
		else if (line == STATUS_DONE)
		{
			return true;
		}
		else if (line == STATUS_ERROR)
		{
			logHost("Error");
			return false;
		}
		else
		{
			if (line.find("Write page") != 0)
			{
				logDevice(line);
			}
		}
	}
	return false;
}

std::vector<uint8_t> readFile(const std::string& filename)
{
	std::ifstream file(filename, std::ios::binary);

	return std::vector<uint8_t>((std::istreambuf_iterator<char>(file)),
			std::istreambuf_iterator<char>());
}

bool executeWriteDataCommand(serial::Serial& serialConnection)
{
	logHost("Uploading data");

	std::string fileName = "output.new.bin";
	logHost("Reading memory from \"" + fileName + "\" file");

	std::vector<uint8_t> data = readFile(fileName);

	if (data.size() != EXPECTED_SIZE)
	{
		logHost(
				"Error: Size of \"" + fileName + "\' file is incorrect: " + std::to_string(data.size()) + ". Expected: "
						+ std::to_string(EXPECTED_SIZE));
		return false;
	}
	assert((data.size() % PAGE_SIZE) == 0);

	logHost("Starting upload...");

	for (int i = 0; i < (int) data.size(); i += PAGE_SIZE)
	{
		int pageNum = i / PAGE_SIZE;

		if (!writePage(serialConnection, pageNum, &data[i], PAGE_SIZE))
		{
			return false;
		}
		int bytes = i + PAGE_SIZE;

		double prc = 100.0 * bytes / EXPECTED_SIZE;

		std::ostringstream oss;
		oss << std::fixed << std::setprecision(1) << prc << "% Page " << (pageNum + 1) << '/' << PAGE_COUNT << " ("
				<< data.size() << " bytes)";

		showProgress(oss.str());
	}
	std::cout << std::endl;

	return true;
}

bool executeChangeBaudRateCommand(serial::Serial& serialConnection, int baudRate)
{
	sendCommand(serialConnection, "baud:" + std::to_string(baudRate));

	while (true)
	{
		std::string line = getLine(serialConnection);

		if (line == STATUS_PROCEED)
		{
			serialConnection.flush();
			serialConnection.setBaudrate(baudRate);
		}
		else if (line == STATUS_DONE)
		{
			return true;
		}
		else if (line == STATUS_ERROR)
		{
			logHost("Error");
			return false;
		}
		else
		{
			logDevice(line);
		}
	}
	return false;
}

int run(int argc, char** argv)
{
	std::vector<serial::PortInfo> devicesFound = serial::list_ports();
	printPorts(devicesFound);

	std::string comPort = COM_PORT;

	for (const serial::PortInfo& portInfo : devicesFound)
	{
		if (portInfo.description.find("CH340") != std::string::npos)
		{
			comPort = portInfo.port;
			break;
		}
	}
	logHost("Connection to port: " + comPort);
	logHost("Baud rate: " + std::to_string(BAUD_RATE));

	serial::Serial serialConnection(comPort, BAUD_RATE, serial::Timeout::simpleTimeout(1000));

	if (!serialConnection.isOpen())
	{
		std::cout << "Failed to open connection for port '" << COM_PORT << "'." << std::endl;
		return -1;
	}

	while (true)
	{
		std::string line = getLine(serialConnection);

		if (line == STATUS_READY)
		{
			if (!measureTime(executeChangeBaudRateCommand, serialConnection, 400000))
			{
				break;
			}
			if (!measureTime(executeEraseAllCommand, serialConnection))
			{
				break;
			}
			if (!measureTime(executeWriteDataCommand, serialConnection))
			{
				break;
			}
			if (!measureTime(executeReadAllCommand, serialConnection))
			{
				break;
			}
//			if (!measureTime(executeReadPageCommand, serialConnection, 0))
//			{
//				break;
//			}
//			if (!measureTime(executeEraseSectorCommand, serialConnection, 0))
//			{
//				break;
//			}
			logHost("Success");
			break;
		}
		else if (line == STATUS_ERROR)
		{
			logHost("Error");
			break;
		}
		else
		{
			logDevice(line);
		}
	}

	return 0;
}

int main(int argc, char** argv)
{
	try
	{
		return run(argc, argv);
	} catch (std::exception &e)
	{
		std::cerr << "Unhandled Exception: " << e.what() << std::endl;
	}
}
