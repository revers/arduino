// http://gammon.com.au/i2c

// Written by Nick Gammon
// February 2011

// Note: maximum address is 0x7FFF (32 Kbytes)

#include <Wire.h>     

const byte rom = 0x50;    // Address of 24LC256 eeprom chip

void setup (void)
  {
  Serial.begin (9600);   // debugging

  Wire.begin ();   

  // test: write the number 123 to address 0x1234
  writeEEPROM (rom, 0x1234, 123);
  
  // read back to confirm
  byte a = readEEPROM (rom, 0x1234);
 
  Serial.println (a, DEC);  // display to confirm
 
  // test: write a string to address 0x1000
  byte hello [] = "Hello, world!";
  
  writeEEPROM (rom, 0x1000, hello, sizeof hello);
  
  // read back to confirm
  byte test [sizeof hello];
  byte err = readEEPROM (rom, 0x1000, test, sizeof test);

  Serial.println ((char *) test);  // display to confirm
  }  // end of setup

void loop() {}  // no main loop


// write len (max 32) bytes to device, returns non-zero on error
//  return code: 0xFF means buffer too long
//             other: other error (eg. device not present)

// Note that if writing multiple bytes the address plus
//  length must not cross a 64-byte boundary or it will "wrap"

byte writeEEPROM (byte device, unsigned int addr, byte * data, byte len ) 
  {
  byte err;
  byte counter;

  if (len > BUFFER_LENGTH)  // 32 (in Wire.h)
    return 0xFF;  // too long

  Wire.beginTransmission(device);
  Wire.write ((byte) (addr >> 8));    // high order byte
  Wire.write ((byte) (addr & 0xFF));  // low-order byte
  Wire.write (data, len);
  err = Wire.endTransmission ();
  
  if (err != 0)
    return err;  // cannot write to device
    
  // wait for write to finish by sending address again
  //  ... give up after 100 attempts (1/10 of a second)
  for (counter = 0; counter < 100; counter++)
    {
    delayMicroseconds (300);  // give it a moment
    Wire.beginTransmission (device);
    Wire.write ((byte) (addr >> 8));    // high order byte
    Wire.write ((byte) (addr & 0xFF));  // low-order byte
    err = Wire.endTransmission ();
    if (err == 0)
      break;
    }
   
  return err;
 
  } // end of writeEEPROM

// write one byte to device, returns non-zero on error
byte writeEEPROM (byte device, unsigned int addr, byte data ) 
  {
  return writeEEPROM (device, addr, &data, 1);
  } // end of writeEEPROM

// read len (max 32) bytes from device, returns non-zero on error
//  return code: 0xFF means buffer too long
//               0xFE means device did not return all requested bytes
//             other: other error (eg. device not present)

// Note that if reading multiple bytes the address plus
//  length must not cross a 64-byte boundary or it will "wrap"

byte readEEPROM (byte device, unsigned int addr, byte * data, byte len ) 
  {
  byte err;
  byte counter;

  if (len > BUFFER_LENGTH)  // 32 (in Wire.h)
    return 0xFF;  // too long

  Wire.beginTransmission (device);
  Wire.write ((byte) (addr >> 8));    // high order byte
  Wire.write ((byte) (addr & 0xFF));  // low-order byte
  err = Wire.endTransmission ();
  
  if (err != 0)
    return err;  // cannot read from device
  
  // initiate blocking read into internal buffer
  Wire.requestFrom (device, len);

  // pull data out of Wire buffer into our buffer
  for (counter = 0; counter < len; counter++)
    {
    data [counter] = Wire.read ();
    }
    
  return 0;  // OK
  }  // end of readEEPROM

// read one byte from device, returns 0xFF on error
byte readEEPROM (byte device, unsigned int addr ) 
  {
  byte temp;

  if (readEEPROM (device, addr, &temp, 1) == 0)
    return temp;

  return 0xFF;

  }  // end of readEEPROM
