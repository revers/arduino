#include <LiquidCrystal.h>

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

float temp;

#define LM35 0

void setup() {
  lcd.begin(16, 2);
  lcd.setCursor(0, 0);
  lcd.print("TEMPERATURA");
}

void loop() {
  temp = analogRead(LM35);
  temp *= 0.48828125f;
  
  lcd.setCursor(0, 1);
  lcd.print(temp);
  lcd.print(" *C");
  
  delay(1000);
}
