#include <LiquidCrystal.h>
#include <Keypad.h>

#define ROWS 4
#define COLS 4

char keys[ROWS][COLS] = {
  { '1', '2', '3', 'A' },
  { '4', '5', '6', 'B' },
  { '7', '8', '9', 'C' },
  { '*', '0', '#', 'D' }
};

byte rowPins[ROWS] = { 22, 23, 24, 25 };
byte colPins[COLS] = { 26, 27, 28, 29 };

Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS);

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

void setup() {
  lcd.begin(16, 2);
  lcd.setCursor(0, 0);
  lcd.print("Keypad");
}

void loop() {
  char key = keypad.getKey();

  if (key) {
    lcd.setCursor(0, 1);
    lcd.print("         ");    
    lcd.setCursor(0, 1);
    lcd.print(key);
  }
}
