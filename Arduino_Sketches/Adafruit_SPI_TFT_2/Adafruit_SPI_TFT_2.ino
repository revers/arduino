// Written by Nick Gammon
// April 2011

#include <SPI.h>

#include <Arduino.h>
//#include "gfxfont.h"

// Adafruit_HX8357 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#define HX8357D                    0xD  ///< Our internal const for D type
#define HX8357B                    0xB  ///< Our internal const for B type

#define HX8357_TFTWIDTH            320  ///< 320 pixels wide
#define HX8357_TFTHEIGHT           480  ///< 480 pixels tall

#define HX8357_NOP                0x00  ///< No op
#define HX8357_SWRESET            0x01  ///< software reset
#define HX8357_RDDID              0x04  ///< Read ID
#define HX8357_RDDST              0x09  ///< (unknown)

#define HX8357_RDPOWMODE          0x0A  ///< Read power mode Read power mode
#define HX8357_RDMADCTL           0x0B  ///< Read MADCTL
#define HX8357_RDCOLMOD           0x0C  ///< Column entry mode
#define HX8357_RDDIM              0x0D  ///< Read display image mode
#define HX8357_RDDSDR             0x0F  ///< Read dosplay signal mode

#define HX8357_SLPIN              0x10  ///< Enter sleep mode
#define HX8357_SLPOUT             0x11  ///< Exit sleep mode
#define HX8357B_PTLON             0x12  ///< Partial mode on
#define HX8357B_NORON             0x13  ///< Normal mode

#define HX8357_INVOFF             0x20  ///< Turn off invert
#define HX8357_INVON              0x21  ///< Turn on invert
#define HX8357_DISPOFF            0x28  ///< Display on
#define HX8357_DISPON             0x29  ///< Display off

#define HX8357_CASET              0x2A  ///< Column addr set
#define HX8357_PASET              0x2B  ///< Page addr set
#define HX8357_RAMWR              0x2C  ///< Write VRAM
#define HX8357_RAMRD              0x2E  ///< Read VRAm

#define HX8357B_PTLAR             0x30  ///< (unknown)
#define HX8357_TEON               0x35  ///< Tear enable on
#define HX8357_TEARLINE           0x44  ///< (unknown)
#define HX8357_MADCTL             0x36  ///< Memory access control
#define HX8357_COLMOD             0x3A  ///< Color mode

#define HX8357_SETOSC             0xB0  ///< Set oscillator
#define HX8357_SETPWR1            0xB1  ///< Set power control
#define HX8357B_SETDISPLAY        0xB2  ///< Set display mode
#define HX8357_SETRGB             0xB3  ///< Set RGB interface
#define HX8357D_SETCOM            0xB6  ///< Set VCOM voltage

#define HX8357B_SETDISPMODE       0xB4  ///< Set display mode
#define HX8357D_SETCYC            0xB4  ///< Set display cycle reg
#define HX8357B_SETOTP            0xB7  ///< Set OTP memory
#define HX8357D_SETC              0xB9  ///< Enable extension command

#define HX8357B_SET_PANEL_DRIVING 0xC0  ///< Set panel drive mode
#define HX8357D_SETSTBA           0xC0  ///< Set source option
#define HX8357B_SETDGC            0xC1  ///< Set DGC settings
#define HX8357B_SETID             0xC3  ///< Set ID
#define HX8357B_SETDDB            0xC4  ///< Set DDB
#define HX8357B_SETDISPLAYFRAME   0xC5  ///< Set display frame
#define HX8357B_GAMMASET          0xC8  ///< Set Gamma correction
#define HX8357B_SETCABC           0xC9  ///< Set CABC
#define HX8357_SETPANEL           0xCC  ///< Set Panel

#define HX8357B_SETPOWER          0xD0  ///< Set power control
#define HX8357B_SETVCOM           0xD1  ///< Set VCOM
#define HX8357B_SETPWRNORMAL      0xD2  ///< Set power normal

#define HX8357B_RDID1             0xDA  ///< Read ID #1
#define HX8357B_RDID2             0xDB  ///< Read ID #2
#define HX8357B_RDID3             0xDC  ///< Read ID #3
#define HX8357B_RDID4             0xDD  ///< Read ID #4

#define HX8357D_SETGAMMA          0xE0  ///< Set Gamma

#define HX8357B_SETGAMMA          0xC8 ///< Set Gamma
#define HX8357B_SETPANELRELATED   0xE9 ///< Set panel related

#define MADCTL_MY  0x80 ///< Bottom to top
#define MADCTL_MX  0x40 ///< Right to left
#define MADCTL_MV  0x20 ///< Reverse Mode
#define MADCTL_ML  0x10 ///< LCD refresh Bottom to top
#define MADCTL_RGB 0x00 ///< Red-Green-Blue pixel order
#define MADCTL_BGR 0x08 ///< Blue-Green-Red pixel order
#define MADCTL_MH  0x04 ///< LCD refresh right to left

// Plan is to move this to GFX header (with different prefix), though
// defines will be kept here for existing code that might be referencing
// them. Some additional ones are in the ILI9341 lib -- add all in GFX!
// Color definitions
#define	HX8357_BLACK   0x0000 ///< BLACK color for drawing graphics
#define	HX8357_BLUE    0x001F ///< BLUE color for drawing graphics
#define	HX8357_RED     0xF800 ///< RED color for drawing graphics
#define	HX8357_GREEN   0x07E0 ///< GREEN color for drawing graphics
#define HX8357_CYAN    0x07FF ///< CYAN color for drawing graphics
#define HX8357_MAGENTA 0xF81F ///< MAGENTA color for drawing graphics
#define HX8357_YELLOW  0xFFE0 ///< YELLOW color for drawing graphics
#define HX8357_WHITE   0xFFFF ///< WHITE color for drawing graphics

static const uint8_t PROGMEM initd[] =
		{
		HX8357_SWRESET, 0x80 + 100 / 5, // Soft reset, then delay 10 ms
		HX8357D_SETC, 3,
				0xFF, 0x83, 0x57,
				0xFF, 0x80 + 500 / 5,          // No command, just delay 300 ms
				HX8357_SETRGB, 4,
				0x80, 0x00, 0x06, 0x06,    // 0x80 enables SDO pin (0x00 disables)
				HX8357D_SETCOM, 1,
				0x25,                      // -1.52V
				HX8357_SETOSC, 1,
				0x68,                      // Normal mode 70Hz, Idle mode 55 Hz
				HX8357_SETPANEL, 1,
				0x05,                      // BGR, Gate direction swapped
				HX8357_SETPWR1, 6,
				0x00,                      // Not deep standby
				0x15,                      // BT
				0x1C,                      // VSPR
				0x1C,                      // VSNR
				0x83,                      // AP
				0xAA,                      // FS
				HX8357D_SETSTBA, 6,
				0x50,                      // OPON normal
				0x50,                      // OPON idle
				0x01,                      // STBA
				0x3C,                      // STBA
				0x1E,                      // STBA
				0x08,                      // GEN
				HX8357D_SETCYC, 7,
				0x02,                      // NW 0x02
				0x40,                      // RTN
				0x00,                      // DIV
				0x2A,                      // DUM
				0x2A,                      // DUM
				0x0D,                      // GDON
				0x78,                      // GDOFF
				HX8357D_SETGAMMA, 34,
				0x02, 0x0A, 0x11, 0x1d, 0x23, 0x35, 0x41, 0x4b, 0x4b,
				0x42, 0x3A, 0x27, 0x1B, 0x08, 0x09, 0x03, 0x02, 0x0A,
				0x11, 0x1d, 0x23, 0x35, 0x41, 0x4b, 0x4b, 0x42, 0x3A,
				0x27, 0x1B, 0x08, 0x09, 0x03, 0x00, 0x01,
				HX8357_COLMOD, 1,
				0x55,                      // 16 bit
				HX8357_MADCTL, 1,
				0xC0,
				HX8357_TEON, 1,
				0x00,                      // TW off
				HX8357_TEARLINE, 2,
				0x00, 0x02,
				HX8357_SLPOUT, 0x80 + 150 / 5, // Exit Sleep, then delay 150 ms
				HX8357_DISPON, 0x80 + 50 / 5, // Main screen turn on, delay 50 ms
				0,                           // END OF COMMAND LIST
		};

#define MY_CS    10  // raspberry: 0
#define MY_DC    9   // raspberry: 2

///< Display width as modified by current rotation
int16_t _width = HX8357_TFTWIDTH;

///< Display height as modified by current rotation
int16_t _height = HX8357_TFTHEIGHT;

int myInitSPI()
{
#ifdef RASPBERRY
	const int channel = 0;
	const int speed = 16000000;
	int fd = 0;

	if ((fd = wiringPiSPISetup(channel, speed)) < 0)
	{
		return fd;
	}
#else

	// Put SCK, MOSI, SS pins into output mode
	// also put SCK, MOSI into LOW state, and SS into HIGH state.
	// Then put SPI hardware into Master mode and turn SPI on
	SPI.begin();

	// Slow down the master a bit
	SPI.setClockDivider(SPI_CLOCK_DIV2);
#endif

	// Init basic control pins common to all connection types
	pinMode(MY_CS, OUTPUT);
	digitalWrite(MY_CS, HIGH); // Deselect

	pinMode(MY_DC, OUTPUT);
	digitalWrite(MY_DC, HIGH); // Data mode

	return 0;
}

uint8_t mySpiWrite(uint8_t data)
{
	return SPI.transfer(data);
}

uint8_t mySpiRead()
{
	return SPI.transfer((uint8_t) 0);
}

void mySpiBeginTransaction()
{
//	SPI.beginTransaction(mySpiSettings);
}

void mySpiEndTransaction()
{
	//SPI.endTransaction();
}

void myStartWrite()
{
	mySpiBeginTransaction();
	digitalWrite(MY_CS, LOW); // select
}

void myEndWrite()
{
	digitalWrite(MY_CS, HIGH); // Deselect
	mySpiEndTransaction();
}

int myInit()
{
//#define DO_LOG
	int err = 0;

	if ((err = myInitSPI()) != 0)
	{
		return err;
	}
	const uint8_t *addr = initd;

	uint8_t cmd, x, numArgs;
	while ((cmd = pgm_read_byte(addr++)) > 0)
	{ // '0' command ends list
		x = pgm_read_byte(addr++);
		numArgs = x & 0x7F;

		if (cmd != 0xFF)
		{ // '255' is ignored
			myStartWrite();

			digitalWrite(MY_DC, LOW); // Command mode
			mySpiWrite(cmd);

#ifdef DO_LOG
			Serial.print("Command: ");
			Serial.print(cmd, HEX);
#endif
			digitalWrite(MY_DC, HIGH); // Data mode

			if ((x & 0x80) == 0) // // If high bit set, numArgs is a delay time
			{
#ifdef DO_LOG
				Serial.print(", numArgs: ");
				Serial.print(numArgs);
				Serial.print(", args: ");
#endif
				for (int i = 0; i < numArgs; i++)
				{
					uint8_t arg = pgm_read_byte(addr + i);
					mySpiWrite(arg); // Send the data bytes

#ifdef DO_LOG
					Serial.print(arg, HEX);
					Serial.print(", ");
#endif
				}

				addr += numArgs;
			}
			myEndWrite();
		}
		if (x & 0x80)
		{       // If high bit set...
			delay(numArgs * 5); // numArgs is actually a delay time (5ms units)
#ifdef DO_LOG
					Serial.print(", delay: ");
					Serial.print(numArgs * 5);
					Serial.print(" ms");
#endif
		}
#ifdef DO_LOG
		Serial.println();
#endif
	}
	return err;

#undef DO_LOG
}

/*!
 @brief   Read 8 bits of data from display configuration memory (not RAM).
 This is highly undocumented/supported and should be avoided,
 function is only included because some of the examples use it.
 @param   commandByte
 The command register to read data from.
 @param   index
 The byte index into the command to read from.
 @return  Unsigned 8-bit data read from display register.
 */
/**************************************************************************/
uint8_t myReadCommand(uint8_t commandByte, uint8_t index = 0)
{
	uint8_t result;

	myStartWrite();

	digitalWrite(MY_DC, LOW); // Command mode
	mySpiWrite(commandByte);
	digitalWrite(MY_DC, HIGH); // Data mode
	do
	{
		result = mySpiRead();
	} while (index--); // Discard bytes up to index'th

	myEndWrite();

	return result;
}

void mySendCommand1Arg(uint8_t commandByte, uint8_t data)
{
	myStartWrite();

	digitalWrite(MY_DC, LOW); // Command mode
	mySpiWrite(commandByte);
	digitalWrite(MY_DC, HIGH); // Data mode
	mySpiWrite(data);

	myEndWrite();
}

void mySendCommand(uint8_t commandByte)
{
	myStartWrite();

	digitalWrite(MY_DC, LOW); // Command mode
	mySpiWrite(commandByte);
	digitalWrite(MY_DC, HIGH); // Data mode

	myEndWrite();
}

/*!
 @brief   Set origin of (0,0) and orientation of TFT display
 @param   m
 The index for rotation, from 0-3 inclusive
 @return  None (void).
 */
void mySetRotation(uint8_t m)
{
	uint8_t rotation = m & 3; // can't be higher than 3

	switch (rotation)
	{
	case 0:
		m = MADCTL_MX | MADCTL_MY | MADCTL_RGB;
		_width = HX8357_TFTWIDTH;
		_height = HX8357_TFTHEIGHT;
		break;
	case 1:
		m = MADCTL_MV | MADCTL_MY | MADCTL_RGB;
		_width = HX8357_TFTHEIGHT;
		_height = HX8357_TFTWIDTH;
		break;
	case 2:
		m = MADCTL_RGB;
		_width = HX8357_TFTWIDTH;
		_height = HX8357_TFTHEIGHT;
		break;
	case 3:
		m = MADCTL_MX | MADCTL_MV | MADCTL_RGB;
		_width = HX8357_TFTHEIGHT;
		_height = HX8357_TFTWIDTH;
		break;
	}
	mySendCommand1Arg(HX8357_MADCTL, m);
}

void MY_SPI_WRITE16(uint16_t val)
{
	uint8_t hi = val >> 8;
	uint8_t lo = val;

	mySpiWrite(hi);
	mySpiWrite(lo);
}

/*!
 @brief   Set the "address window" - the rectangle we will write to
 graphics RAM with the next chunk of SPI data writes. The
 HX8357 will automatically wrap the data as each row is filled.
 @param   x1
 Leftmost column of rectangle (screen pixel coordinates).
 @param   y1
 Topmost row of rectangle (screen pixel coordinates).
 @param   w
 Width of rectangle.
 @param   h
 Height of rectangle.
 @return  None (void).
 */
void mySetAddrWindow(uint16_t x1, uint16_t y1, uint16_t w, uint16_t h)
{
	uint16_t x2 = (x1 + w - 1),
			y2 = (y1 + h - 1);

	myStartWrite();

	digitalWrite(MY_DC, LOW); // Command mode
	mySpiWrite(HX8357_CASET); // Column address set
	digitalWrite(MY_DC, HIGH); // Data mode

	MY_SPI_WRITE16(x1);
	MY_SPI_WRITE16(x2);

	digitalWrite(MY_DC, LOW); // Command mode
	mySpiWrite(HX8357_PASET); // Row address set
	digitalWrite(MY_DC, HIGH); // Data mode

	MY_SPI_WRITE16(y1);
	MY_SPI_WRITE16(y2);

	digitalWrite(MY_DC, LOW); // Command mode
	mySpiWrite(HX8357_RAMWR); // Write to RAM
	digitalWrite(MY_DC, HIGH); // Data mode

	myEndWrite();
}



/*!
 @brief  Issue a series of pixels, all the same color. Not self-
 contained; should follow startWrite() and setAddrWindow() calls.
 @param  color  16-bit pixel color in '565' RGB format.
 @param  len    Number of pixels to draw.
 */
void myWriteColor(uint16_t color, uint32_t len)
{
	if (!len)
		return; // Avoid 0-byte transfers

	uint8_t hi = color >> 8;
	uint8_t lo = color;

	myStartWrite();

	while (len--)
	{

		mySpiWrite(hi);
		mySpiWrite(lo);
	}

	myEndWrite();
}

/*!
 @brief  A lower-level version of writeFillRect(). This version requires
 all inputs are in-bounds, that width and height are positive,
 and no part extends offscreen. NO EDGE CLIPPING OR REJECTION IS
 PERFORMED. If higher-level graphics primitives are written to
 handle their own clipping earlier in the drawing process, this
 can avoid unnecessary function calls and repeated clipping
 operations in the lower-level functions.
 @param  x      Horizontal position of first corner. MUST BE WITHIN
 SCREEN BOUNDS.
 @param  y      Vertical position of first corner. MUST BE WITHIN SCREEN
 BOUNDS.
 @param  w      Rectangle width in pixels. MUST BE POSITIVE AND NOT
 EXTEND OFF SCREEN.
 @param  h      Rectangle height in pixels. MUST BE POSITIVE AND NOT
 EXTEND OFF SCREEN.
 @param  color  16-bit fill color in '565' RGB format.
 @note   This is a new function, no graphics primitives besides rects
 and horizontal/vertical lines are written to best use this yet.
 */
inline void myWriteFillRectPreclipped(int16_t x, int16_t y,
		int16_t w, int16_t h, uint16_t color)
{
	mySetAddrWindow(x, y, w, h);
	myWriteColor(color, (uint32_t) w * h);
}

/*!
 @brief  Draw a vertical line on the display. Self-contained and provides
 its own transaction as needed (see writeFastHLine() for a lower-
 level variant). Edge clipping and rejection is performed here.
 @param  x      Horizontal position of first point.
 @param  y      Vertical position of first point.
 @param  h      Line height in pixels (positive = below first point,
 negative = above first point).
 @param  color  16-bit line color in '565' RGB format.
 @note   This repeats the writeFastVLine() function almost in its
 entirety, with the addition of a transaction start/end. It's
 done this way (rather than starting the transaction and calling
 writeFastVLine() to handle clipping and so forth) so that the
 transaction isn't performed at all if the line is rejected.
 */
void myWriteFastVLine(int16_t x, int16_t y, int16_t h,
		uint16_t color)
{
	if ((x >= 0) && (x < _width) && h)
	{ // X on screen, nonzero height
		if (h < 0)
		{                     // If negative height...
			y += h + 1;                //   Move Y to top edge
			h = -h;                    //   Use positive height
		}
		if (y < _height)
		{               // Not off bottom
			int16_t y2 = y + h - 1;
			if (y2 >= 0)
			{               // Not off top
				// Line partly or fully overlaps screen
				if (y < 0)
				{
					y = 0;
					h = y2 + 1;
				} // Clip top
				if (y2 >= _height)
				{
					h = _height - y;
				} // Clip bottom
				myStartWrite();
				myWriteFillRectPreclipped(x, y, 1, h, color);
				myEndWrite();
			}
		}
	}
}

void myWriteFastHLine(int16_t x, int16_t y, int16_t w,
		uint16_t color)
{
	if ((y >= 0) && (y < _height) && w)
	{ // Y on screen, nonzero width
		if (w < 0)
		{                      // If negative width...
			x += w + 1;                 //   Move X to left edge
			w = -w;                     //   Use positive width
		}
		if (x < _width)
		{                 // Not off right
			int16_t x2 = x + w - 1;
			if (x2 >= 0)
			{                // Not off left
				// Line partly or fully overlaps screen
				if (x < 0)
				{
					x = 0;
					w = x2 + 1;
				} // Clip left
				if (x2 >= _width)
				{
					w = _width - x;
				} // Clip right
				myStartWrite();
				myWriteFillRectPreclipped(x, y, w, 1, color);
				myEndWrite();
			}
		}
	}
}

/**************************************************************************/
/*!
 @brief    Fill a rectangle completely with one color. Update in subclasses if desired!
 @param    x   Top left corner x coordinate
 @param    y   Top left corner y coordinate
 @param    w   Width in pixels
 @param    h   Height in pixels
 @param    color 16-bit 5-6-5 Color to fill with
 */
/**************************************************************************/
void myFillRect(int16_t x, int16_t y, int16_t w, int16_t h,
		uint16_t color)
{
	if (w && h)
	{                            // Nonzero width and height?
		if (w < 0)
		{                         // If negative width...
			x += w + 1;                    //   Move X to left edge
			w = -w;                        //   Use positive width
		}
		if (x < _width)
		{                    // Not off right
			if (h < 0)
			{                     // If negative height...
				y += h + 1;                //   Move Y to top edge
				h = -h;                    //   Use positive height
			}
			if (y < _height)
			{               // Not off bottom
				int16_t x2 = x + w - 1;
				if (x2 >= 0)
				{               // Not off left
					int16_t y2 = y + h - 1;
					if (y2 >= 0)
					{           // Not off top
						// Rectangle partly or fully overlaps screen
						if (x < 0)
						{
							x = 0;
							w = x2 + 1;
						} // Clip left
						if (y < 0)
						{
							y = 0;
							h = y2 + 1;
						} // Clip top
						if (x2 >= _width)
						{
							w = _width - x;
						} // Clip right
						if (y2 >= _height)
						{
							h = _height - y;
						} // Clip bottom
						myStartWrite();
						myWriteFillRectPreclipped(x, y, w, h, color);
						myEndWrite();
					}
				}
			}
		}
	}
}

/**************************************************************************/
/*!
 @brief    Fill the screen completely with one color. Update in subclasses if desired!
 @param    color 16-bit 5-6-5 Color to fill with
 */
/**************************************************************************/
void myFillScreen(uint16_t color)
{
	myFillRect(0, 0, _width, _height, color);
}

/**************************************************************************/
/*!
 @brief   Draw a rectangle with no fill color
 @param    x   Top left corner x coordinate
 @param    y   Top left corner y coordinate
 @param    w   Width in pixels
 @param    h   Height in pixels
 @param    color 16-bit 5-6-5 Color to draw with
 */
/**************************************************************************/
void myDrawRect(int16_t x, int16_t y, int16_t w, int16_t h,
		uint16_t color)
{
	myStartWrite();
	myWriteFastHLine(x, y, w, color);
	myWriteFastHLine(x, y + h - 1, w, color);
	myWriteFastVLine(x, y, h, color);
	myWriteFastVLine(x + w - 1, y, h, color);
	myEndWrite();
}

void testRects(uint16_t color)
{
	int n, i, i2,
			cx = _width / 2,
			cy = _height / 2;

	myFillScreen(HX8357_BLACK);

	n = min(_width, _height);

	for (i = 2; i < n; i += 6)
	{
		i2 = i / 2;
		myDrawRect(cx - i2, cy - i2, i, i, color);
	}
}

void setup(void)
{
	Serial.begin(9600);
	Serial.println("HX8357D Test!");

	int err;

	if ((err = myInit()) != 0)
	{
		Serial.print("Error: ");
		Serial.println(err, HEX);
		return;
	}
	// read diagnostics (optional but can help debug problems)
	uint8_t x = myReadCommand(HX8357_RDPOWMODE);
	Serial.print("Display Power Mode: 0x");
	Serial.println(x, HEX);

	x = myReadCommand(HX8357_RDMADCTL);
	Serial.print("MADCTL Mode: 0x");
	Serial.println(x, HEX);
	x = myReadCommand(HX8357_RDCOLMOD);
	Serial.print("Pixel Format: 0x");
	Serial.println(x, HEX);
	x = myReadCommand(HX8357_RDDIM);
	Serial.print("Image Format: 0x");
	Serial.println(x, HEX);
	x = myReadCommand(HX8357_RDDSDR);
	Serial.print("Self Diagnostic: 0x");
	Serial.println(x, HEX);
	Serial.println(F("Done!"));

	mySetRotation(0);
	myFillScreen(HX8357_MAGENTA);
}

void loop(void)
{

	uint16_t colors[] =
			{ HX8357_CYAN, HX8357_MAGENTA, HX8357_YELLOW, HX8357_GREEN };

	for (uint8_t rotation = 0; rotation < 4; rotation++)
	{
		mySetRotation(rotation);

		testRects(HX8357_GREEN);
		delay(500);

		myFillScreen(colors[rotation]);
		delay(1000);
	}
}
