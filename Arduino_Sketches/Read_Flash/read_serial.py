import serial

ser = serial.Serial("COM5", 115200, timeout=0)

try:
    line = []
    while True:
        for c in ser.read():
            if c == '\n':
                print("Line: " + ''.join(line))
                line = []
                break
            else:
                line.append(c)
finally:
    ser.close()