Initialising..........
23:58:57.859 -> Chip Diagnostics initiated.
23:58:57.859 -> 
23:58:57.859 -> Capcity id = 19
23:58:57.859 -> No Chip size defined by user. Automated identification initiated.
23:58:57.859 -> Chip identified. This chip is fully supported by the library.
23:58:57.859 -> _pageSize: 0
23:58:57.859 -> 
23:58:57.859 -> SPIMemory Library version: 3.2.1
23:58:57.859 -> 
23:58:57.859 -> JEDEC ID: 0x202013
23:58:57.859 -> Man ID: 0x20
23:58:57.859 -> Memory ID: 0x20
23:58:57.859 -> Capacity: 524288
23:58:57.892 -> Max Pages: 4294967295
23:58:57.892 -> 
23:58:57.892 -> -----------------------------------------------------------------------------------------------------------------------------
23:58:57.892 -> 							Testing library code
23:58:57.892 -> -----------------------------------------------------------------------------------------------------------------------------
23:58:57.892 -> 			Function		Test result			     Runtime
23:58:57.892 -> -----------------------------------------------------------------------------------------------------------------------------
23:58:57.926 -> 			Power Down		   PASS				      100 us
23:58:57.926 -> 			Power Up		   PASS				      80 us
23:58:57.960 -> 
23:58:57.960 -> 			Erase Chip		   PASS				      3.588 s
23:59:01.578 -> 			Erase 72KB		   PASS				      84 us
23:59:01.612 -> 			Erase 64KB		   PASS				      574.22 ms
23:59:02.222 -> 			Erase 32KB		   PASS				      156 us
23:59:02.222 -> 			Erase 4KB		   PASS				      136 us
23:59:02.256 -> -----------------------------------------------------------------------------------------------------------------------------
23:59:02.256 -> 			Data type		I/O Result	      Write time	      Read time
23:59:02.256 -> -----------------------------------------------------------------------------------------------------------------------------
23:59:02.290 -> 			Byte			   PASS			184 us			76 us
23:59:02.324 -> 			Char			   PASS			168 us			68 us
23:59:02.359 -> 			Word			   PASS			176 us			68 us
23:59:02.392 -> 			Short			   PASS			176 us			72 us
23:59:02.426 -> 			ULong			   PASS			212 us			76 us
23:59:02.493 -> 			Long			   PASS			216 us			72 us
23:59:02.526 -> 			Float			   PASS			224 us			76 us
23:59:02.560 -> 			Struct			   PASS			356 us			112 us
23:59:02.593 -> 			Byte Array		   PASS			3.09 ms			420 us
23:59:02.661 -> 			String			   PASS			240 us			84 us
23:59:02.695 -> -----------------------------------------------------------------------------------------------------------------------------
