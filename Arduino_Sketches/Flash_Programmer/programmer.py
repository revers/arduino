import sys
import time
import serial
from datetime import datetime

STATUS_PREFIX = '\x01'
STATUS_READY = STATUS_PREFIX + 'READY'
STATUS_ERROR = STATUS_PREFIX + 'ERROR'
STATUS_DONE = STATUS_PREFIX + 'DONE'
STATUS_PROCEED = STATUS_PREFIX + 'GO'
STATUS_MESSAGE = STATUS_PREFIX + ':'

END_OF_DATA = '\x03'
ESCAPE = '\x1B'

EXPECTED_SIZE = 512 * 1024

COM_PORT = "COM5"
# BAUD_RATE = 115200
BAUD_RATE = 200000
PAGE_SIZE = 256
PAGE_COUNT = 2048


def logHost(msg):
    print("  [Host] " + msg)


def logDevice(msg):
    if msg.startswith(STATUS_MESSAGE):
        msg = msg[len(STATUS_MESSAGE):]
    print("[Device] " + repr(msg)[1:-1])


def showProgress(msg):
    sys.stdout.write('\r         ' + msg)


def sendCommand(serialConnection, command):
    logHost("Command: " + command)
    serialConnection.write(command + b';')


def measureTime(func):
    start_time = datetime.now()
    ret = func()
    time_elapsed = datetime.now() - start_time
    logHost('Executed in: {}'.format(time_elapsed))

    return ret
    
    
def printInHex(line):
    counter = 0
    for c in line:
        if counter > 0 and counter % 16 == 0:
            print('')
        counter += 1
        sys.stdout.write(c.encode('hex'))
        
    print('')
    

def getLine(serialConnection):
    lineArray = []

    while True:
        for c in serialConnection.read():
            if c == '\r':
                continue
            if c == '\n':
                line = ''.join(lineArray)
                lineArray = []

                return line
            else:
                lineArray.append(c)
    return None
    
    
def getBinaryLine(serialConnection):
    lineArray = []
    
    doEscape = False
    isStatus = False
    
    while True:
        for c in serialConnection.read():
            if not doEscape and c == '\x1B':
                doEscape = True
                continue
                
            if not doEscape and c == '\n':
                
                line = ''.join(lineArray[:-1]) # -1 because the last element is always \r
                lineArray = []

                return isStatus, line
            else:
                if not lineArray and not doEscape and c == STATUS_PREFIX:
                    # If the very first letter is STATUS_PREFIX then we received a status,
                    # not a binary data. 
                    # Note that in binary data the status character is always escaped.
                    isStatus = True
                    
                doEscape = False
                lineArray.append(c)
    return None


def executeSimpleCommand(serialConnection, command):
    sendCommand(serialConnection, command)

    while True:
        line = getLine(serialConnection)

        if line == STATUS_DONE:
            logHost('Done')
            return True

        elif line == STATUS_ERROR:
            logHost('Error')
            return False

        else:
            logDevice(line)
    return False


def executeEraseAllCommand(serialConnection):
    return executeSimpleCommand(serialConnection, b'erase_all')


def executeEraseSectorCommand(serialConnection, sectorNum):
    return executeSimpleCommand(serialConnection, b'erase_sector:' + str(sectorNum).encode())


def writePage(serialConnection, pageNum, data):
    if len(data) != PAGE_SIZE:
        logHost('Error: Write Page: Data size is incorrect: ' + str(len(data)))
        return False
        
    escapedData = ''
    
    for c in data:
        if c == END_OF_DATA or c == ESCAPE:
            escapedData += ESCAPE
        escapedData += c
        
    serialConnection.write(b'write_page:' + str(pageNum).encode() + b';')

    while True:
        line = getLine(serialConnection)

        if line == STATUS_PROCEED:
            serialConnection.write(escapedData + END_OF_DATA)

        elif line == STATUS_DONE:
            return True

        elif line == STATUS_ERROR:
            logHost('Error')
            return False

        else:
            if not line.startswith('Write page'):
                logDevice(line)
    return False


def executeWriteDataCommand(serialConnection):
    logHost('Uploading data')

    fileName = 'output.orange.bin'
    logHost('Reading memory from "' + fileName + '" file')

    with open(fileName, 'rb') as myfile:
        data = myfile.read()

    if len(data) != EXPECTED_SIZE:
        logHost('Error: "' + fileName + '" size is incorrect: ' + str(len(data)))
        return False

    logHost('Starting upload...')

    bytes = 0

    for pageNum in xrange(PAGE_COUNT):
        startIndex = pageNum * PAGE_SIZE
        partData = data[startIndex:(PAGE_SIZE + startIndex)]

        if not writePage(serialConnection, pageNum, partData):
            return False

        bytes += PAGE_SIZE

        prc = 100.0 * bytes / EXPECTED_SIZE
        showProgress(('%.1f%% ' % prc) + 'Page ' + str(pageNum + 1)
            + '/' + str(PAGE_COUNT) + ' (' + str(bytes) + ' bytes)')
    print('')

    return True
    
    
def executeReadPageCommand(serialConnection, pageNum):
    sendCommand(serialConnection, b'read_page:' + str(pageNum).encode())
    
    while True:
        isStatus, line = getBinaryLine(serialConnection)

        if isStatus:
            if line == STATUS_DONE:
                logHost('Done')
                return True

            elif line == STATUS_ERROR:
                logHost('Error')
                return False

            else:
                logDevice(line)
        else:
            logHost('Response length: ' + str(len(line)))
            printInHex(line)
    return False


def executeReadAllCommand(serialConnection):
    sendCommand(serialConnection, b'read_all')
    data = ''

    while True:
        isStatus, line = getBinaryLine(serialConnection)

        if isStatus:
            if line == STATUS_DONE:
                print('')
                outputTxtFileName = 'output.txt'
                outputBinFileName = 'output.bin'

                logHost('Saving results to ' + outputTxtFileName + ', ' + outputBinFileName)

                with open(outputTxtFileName, 'w') as outputTxtFile, open(outputBinFileName, 'wb') as outputBinFile:
                    for pageNum in xrange(PAGE_COUNT):
                        startIndex = pageNum * PAGE_SIZE
                        partData = data[startIndex:(PAGE_SIZE + startIndex)]

                        hexLine = ''.join(x.encode('hex') for x in partData)
                        outputTxtFile.write(hexLine + '\n')
                    
                    outputBinFile.write(data)

                logHost('Files ' + outputTxtFileName + ', ' + outputBinFileName + ' have been saved')
                
                return True

            elif line == STATUS_ERROR:
                logHost('Error')
                return False
            else:
                logDevice(line)

        else:
            data += line

            bytes = len(data)
            prc = 100.0 * bytes / EXPECTED_SIZE
            showProgress(('%.1f%% (' % prc) + str(bytes) + ' bytes)')
    return False
    
    
def executeChangeBaudRateCommand(serialConnection, baudRate):
    serialConnection.write(b'baud:' + str(baudRate).encode() + b';')

    while True:
        line = getLine(serialConnection)

        if line == STATUS_PROCEED:
            serialConnection.flush()
            serialConnection.baudrate = baudRate

        elif line == STATUS_DONE:
            return True

        elif line == STATUS_ERROR:
            logHost('Error')
            return False

        else:
            logDevice(line)
    return False
    
    
def hexToInt(c):
    c = ord(c)
    if c >= ord('0') and c <= ord('9'):
		return (c - ord('0'));

    if c >= ord('a') and c <= ord('f'):
		return (c - ord('a')) + 10
	
    if c >= ord('A') and c <= ord('F'):
		return (c - ord('A')) + 10;

    raise RuntimeError('Error: Invalid hex character: ' + chr(c))
    

def validateHexDigit(i):
    if i < 0 or i >= 16:
        raise RuntimeError('Invalid hex digit: ' + str(i))
        
    
def convertToBinaries():
    fileName = 'output.blue.txt'
    logHost('Reading memory from "' + fileName + '" file')

    with open(fileName, 'r') as myfile:
        data = myfile.read().replace('\n', '')

    if len(data) != EXPECTED_SIZE * 2:
        logHost('Error: "' + fileName + '" size is incorrect: ' + str(len(data)))
        return False
        
    result = ''

    for i in xrange(EXPECTED_SIZE):
        c1 = data[i * 2 + 0]
        c2 = data[i * 2 + 1]
        
        i1 = hexToInt(c1)
        i2 = hexToInt(c2)
        
        validateHexDigit(i1)
        validateHexDigit(i2)

        result += chr(i1 * 16 + i2)

    outputBinFileName = 'output.blue.bin'

    logHost('Writing file ' + outputBinFileName)
    
    with open(outputBinFileName, 'wb') as outputBinFile:
        outputBinFile.write(result)
        
        
def convertStringToBinary(line):
    length = len(line)
    
    if length % 2 != 0:
        raise RuntimeError('Line length must be even. Length = ' + str(length))
        
    result = ''
        
    for i in xrange(length / 2):
        c1 = line[i * 2 + 0]
        c2 = line[i * 2 + 1]
        
        i1 = hexToInt(c1)
        i2 = hexToInt(c2)
        
        validateHexDigit(i1)
        validateHexDigit(i2)
        
        result += chr(i1 * 16 + i2)
        
    return result
    

def main():
    # converted = convertStringToBinary('04ac886abf10270000')
    #print('Converted: ' + repr(converted))
    #convertToBinaries()
    #return
    serialConnection = serial.Serial(COM_PORT, BAUD_RATE, timeout=0)
    try:
        while True:
            line = getLine(serialConnection)

            if line == STATUS_READY:
                # if not measureTime(lambda: executeChangeBaudRateCommand(serialConnection, 400000)):
                  # break
                if not measureTime(lambda: executeEraseAllCommand(serialConnection)):
                  break
                if not measureTime(lambda: executeWriteDataCommand(serialConnection)):
                  break
                #if not measureTime(lambda: executeReadAllCommand(serialConnection)):
                # break

                #if not measureTime(lambda: executeEraseSectorCommand(serialConnection, 7)):
                #   break
                # if not measureTime(lambda: executeReadPageCommand(serialConnection, 0)):
                 # break

                logHost('Success')
                break

            elif line == STATUS_ERROR:
                logHost('Error')
                break

            else:
                logDevice(line)

    finally:
        serialConnection.close()


if __name__== "__main__":
  main()