#include <SPIMemory.h>

//#define BAUD_RATE   115200
#define BAUD_RATE   200000
#define PAGE_SIZE   256
#define PAGE_COUNT  2048

#define STATUS_PREFIX      '\x01'
#define STATUS_PREFIX_STR  "\x01"
#define END_OF_DATA        '\x03'
#define ESCAPE             '\x1B'

#define STATUS_MESSAGE   STATUS_PREFIX_STR ":"
#define F_MSG(msg)       F(STATUS_MESSAGE msg)



struct Global
{
	SPIFlash flash;
	bool isCommandReady = false;
	String command;

	uint8_t pageBuffer[PAGE_SIZE];

	struct WritePage
	{
		int pageNum = -1;
		bool isEscaped = false;
		int bufSize = 0;
		int overflow = 0;

		void clear() {
			pageNum = -1;
			isEscaped = false;
			bufSize = 0;
			overflow = 0;
		}

	} writePage;

} global;

void sendStatusReady() {
	Serial.println(F(STATUS_PREFIX_STR "READY"));
}

void sendStatusDone() {
	Serial.println(F(STATUS_PREFIX_STR "DONE"));
}

void sendStatusError() {
	Serial.println(F(STATUS_PREFIX_STR "ERROR"));
}

void sendStatusProceed() {
	Serial.println(F(STATUS_PREFIX_STR "GO"));
}

void printPageBytes(byte* pageBuf) {
	char c;
	for (int j = 0; j < PAGE_SIZE; ++j) {
		c = (char) pageBuf[j];

		if (c == '\n' || c == ESCAPE || c == STATUS_PREFIX) {
			Serial.print(ESCAPE);
		}
		Serial.print(c);
	}
	Serial.println();
}

void readAllPages() {
	Serial.println(F_MSG("Reading whole memory"));

	bool result = true;

	for (int i = 0; i < PAGE_COUNT; ++i) {
		result |= global.flash.readByteArray(i * PAGE_SIZE, global.pageBuffer, PAGE_SIZE);
		printPageBytes(global.pageBuffer);
	}
	if (result != true) {
		sendStatusError();
	} else {
		sendStatusDone();
	}
}

bool getID() {
	uint32_t JEDEC = global.flash.getJEDECID();

	if (!JEDEC) {
		Serial.println(F_MSG("Error: No comms. Check wiring."));
		return false;
	} else {
		Serial.print(F_MSG("JEDEC ID: 0x"));
		Serial.println(JEDEC, HEX);
		Serial.print(F_MSG("Man ID: 0x"));
		Serial.println(uint8_t(JEDEC >> 16), HEX);
		Serial.print(F_MSG("Memory ID: 0x"));
		Serial.println(uint8_t(JEDEC >> 8), HEX);
		Serial.print(F_MSG("Capacity: "));
		Serial.println(global.flash.getCapacity());
	}
	return true;
}

void chipErase() {
	Serial.println(F_MSG("Erasing whole chip..."));

	if (!global.flash.eraseChip()) {
		Serial.println(F_MSG("Error: Failed to erase chip."));
		sendStatusError();
	} else {
		Serial.println(F_MSG("Chip erased successfully."));
		sendStatusDone();
	}
}

void writePage(uint32_t pageNum) {
	if (global.writePage.overflow > 0) {
		Serial.print(F_MSG("Error: Too many bytes received: "));
		Serial.print(global.writePage.bufSize + global.writePage.overflow);
		Serial.print(F(". Expected: "));
		Serial.println(PAGE_SIZE);

		sendStatusError();
		return;
	}

	if (global.writePage.bufSize < PAGE_SIZE) {
		Serial.print(F_MSG("Error: Not enough bytes received: "));
		Serial.print(global.writePage.bufSize);
		Serial.print(F(". Expected: "));
		Serial.println(PAGE_SIZE);

		sendStatusError();
		return;
	}
	if (global.flash.writeByteArray(pageNum * PAGE_SIZE, global.pageBuffer, PAGE_SIZE)) {
		sendStatusDone();
	} else {
		Serial.println(F_MSG("Error: Failed to write data."));
		sendStatusError();
	}
}

void readPage(uint32_t pageNum) {
	Serial.print(F_MSG("Reading page: "));
	Serial.println(pageNum);

	if (!global.flash.readByteArray(pageNum * PAGE_SIZE, global.pageBuffer, PAGE_SIZE)) {
		sendStatusError();
		return;
	}
	printPageBytes(global.pageBuffer);

	sendStatusDone();
}

/**
 * There are 8 sectors. Each 64KB
 */
void eraseSector(uint32_t sectorNum) {
	Serial.print(F_MSG("Erasing page: "));
	Serial.println(sectorNum);

	if (!global.flash.eraseBlock64K(sectorNum * KB(64))) {
		Serial.print(F_MSG("Error: Failed to erase page: "));
		Serial.println(sectorNum);
		sendStatusError();
		return;
	}
	sendStatusDone();
}

void setup() {
	Serial.begin(BAUD_RATE);

	delay(50); //Time to terminal get connected
	Serial.print(F_MSG("Initializing"));

	for (uint8_t i = 0; i < 10; ++i) {
		Serial.print('.');
	}
	Serial.println();

	global.flash.begin();

	if (!getID()) {
		sendStatusError();
		return;
	}
	sendStatusReady();
}

inline bool isWritePageCommandPending() {
	return global.writePage.pageNum >= 0;
}

/*
 * This handy built-in callback function alerts the UNO
 * whenever a serial event occurs. In our case, we check
 * for available input data and concatenate a command
 * string, setting a boolean used by the loop() routine
 * as a dispatch trigger.
 */
void serialEvent() {
	char c;

	while (Serial.available()) {
		c = (char) Serial.read();

		if (isWritePageCommandPending()) {
			if (!global.writePage.isEscaped && c == ESCAPE) {
				global.writePage.isEscaped = true;
			} else {
				if (!global.writePage.isEscaped && c == END_OF_DATA) {
					global.isCommandReady = true;
				} else {
					if (global.writePage.bufSize < PAGE_SIZE) {
						global.pageBuffer[global.writePage.bufSize++] = c;
					} else {
						global.writePage.overflow++;
					}
				}
				global.writePage.isEscaped = false;
			}
		} else {
			if (c == ';') {
				global.isCommandReady = true;
			}
			else {
				global.command += c;
			}
		}
	}
}

void loop() {
	if (global.isCommandReady) {

		if (isWritePageCommandPending()) {
			writePage(global.writePage.pageNum);

			global.writePage.clear();

		} else if (global.command == "read_all") {
			readAllPages();

		} else if (global.command == "erase_all") {
			chipErase();

		} else if (global.command.startsWith(F("write_page:"))) {

			String pageNumStr = global.command.substring(11);

			global.writePage.clear();
			global.writePage.pageNum = pageNumStr.toInt();

			Serial.print(F("Write page: "));
			Serial.println(global.writePage.pageNum);

			global.command = "";
			global.isCommandReady = false;
			sendStatusProceed();

		} else if (global.command.startsWith(F("baud:"))) {

			String baudRateStr = global.command.substring(5);
			Serial.print(F("Changing baud rate to: "));
			Serial.println(baudRateStr);

			sendStatusProceed();
			Serial.flush();

			delay(1000);

			Serial.begin(baudRateStr.toInt());
			Serial.println(F("Baud rate successfully changed."));
			sendStatusDone();

		} else if (global.command.startsWith(F("read_page:"))) {

			String pageNumStr = global.command.substring(10);
			readPage(pageNumStr.toInt());

		} else if (global.command.startsWith(F("erase_sector:"))) {

			String pageNumStr = global.command.substring(11);
			eraseSector(pageNumStr.toInt());

		} else {
			Serial.print(F_MSG("Error: Unknown command: "));
			Serial.println(global.command);
			sendStatusError();
		}
		global.command = "";
		global.isCommandReady = false;
	}

}
