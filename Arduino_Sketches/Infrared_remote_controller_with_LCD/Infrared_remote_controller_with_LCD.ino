// Library: https://github.com/z3t0/Arduino-IRremote

#include <LiquidCrystal.h>
#include <IRremote.h>

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

#define RECV_PIN 6

IRrecv irrecv(RECV_PIN);
decode_results results;

void setup() {
  irrecv.enableIRIn();
  
  lcd.begin(16, 2);
  lcd.setCursor(0, 0);
  lcd.print("Nothing");
  
  lcd.setCursor(0, 1);
  lcd.print("Nothing");
}

#define KEY_ZERO 0xFF6897
#define KEY_ONE  0xFF30CF

void loop() {
  lcd.setCursor(0, 1);

  if (irrecv.decode(&results)) {
    if (results.value == KEY_ZERO) {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Pressed");
      lcd.setCursor(0, 1);
      lcd.print("key 0");
    } else if (results.value == KEY_ONE) {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Pressed");
      lcd.setCursor(0, 1);
      lcd.print("key 1");
    } else if (results.value != 0xFFFFFFFF) {
      lcd.clear();
      lcd.setCursor(0, 1);
      lcd.print(results.value, HEX);
    }
    irrecv.resume();
  }
}
