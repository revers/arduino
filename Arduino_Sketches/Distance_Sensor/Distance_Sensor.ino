#include <LiquidCrystal.h>

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

#define LM35 0
#define PIN_TX 6
#define PIN_RX 7

void setup() {
  lcd.begin(16, 2);
  lcd.setCursor(0, 0);
  lcd.print("Distance");

  pinMode(PIN_TX, OUTPUT);
  pinMode(PIN_RX, INPUT);
}

void loop() {
  digitalWrite(PIN_TX, HIGH);
  delayMicroseconds(10);
  digitalWrite(PIN_TX, LOW);

  long impulseTime = pulseIn(PIN_RX, HIGH);
  int distance = (int) (impulseTime / 58);
  
  if (distance < 5 || distance > 200) {
    lcd.setCursor(0, 1);
    lcd.print("Unavailable");
  } else {
    lcd.setCursor(0, 1);
    lcd.print("            ");
    lcd.setCursor(0, 1);
    lcd.print(distance);
    lcd.print(" cm");
  }
  delay(500);
}
