#include <string>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <vector>

#define PAGE_SIZE   256
#define PAGE_COUNT  2048

#define EXPECTED_SIZE   (PAGE_SIZE * PAGE_COUNT)

std::vector<uint8_t> readFile(const std::string& filename)
{
	std::ifstream file(filename, std::ios::binary);

	return std::vector<uint8_t>((std::istreambuf_iterator<char>(file)),
			std::istreambuf_iterator<char>());
}

struct SimplePage
{
	int32_t a;
	int32_t b;
	int32_t c;
	int32_t d;
};

SimplePage getSimplePage(std::vector<uint8_t>& data, int pageNo)
{
	uint32_t* intData = (uint32_t*) &data[pageNo * PAGE_SIZE];
	uint32_t a = intData[0];
	uint32_t b = intData[1];
	uint32_t c = intData[2];
	uint8_t d = data[pageNo * PAGE_SIZE + 3 * sizeof(uint32_t)];

	return
	{	(int32_t) a, (int32_t) b, (int32_t) c, (int32_t) d};
}

void printPage(char id, std::vector<uint8_t>& data, int pageNo)
{
	std::cout << "Page " << pageNo << ": ";

	for (int i = 0; i < 13; i++)
	{
		if (i != 0)
		{
			std::cout << " ";
		}
		std::cout << std::setfill('0') << std::setw(2) << std::hex
				<< ((uint32_t) data[pageNo * PAGE_SIZE + i]);
	}
	std::cout << std::dec;
	std::cout << std::endl;

	uint32_t* intData = (uint32_t*) &data[pageNo * PAGE_SIZE];
	uint32_t a = intData[0];
	uint32_t b = intData[1];
	uint32_t c = intData[2];
	uint8_t d = data[pageNo * PAGE_SIZE + 3 * sizeof(uint32_t)];

	std::cout << "\t" << id << "_a[4] = " << a << '\n';
	std::cout << "\t" << id << "_b[4] = " << b << '\n';
	std::cout << "\t" << id << "_c[4] = " << c << '\n';
	std::cout << "\t" << id << "_d[1] = " << (int32_t) d << '\n';

	std::cout << "\t" << id << "_c - " << id << "_b = " << (c - b) << "  ->  " << id << "_b + " << (c - b) << " = "
			<< id << "_c\n";
}

void printPageShort(std::vector<uint8_t>& data, int pageNo)
{
	std::cout << "Page " << pageNo << ": ";

	for (int i = 0; i < 9; i++)
	{
		if (i != 0)
		{
			std::cout << " ";
		}
		std::cout << std::setfill('0') << std::setw(2) << std::hex
				<< ((uint32_t) data[pageNo * PAGE_SIZE + i]);
	}
	std::cout << std::dec;
	std::cout << std::endl;

	uint32_t* intData = (uint32_t*) &data[pageNo * PAGE_SIZE + 1];

	uint8_t e = data[pageNo * PAGE_SIZE];
	uint32_t f = intData[0];
	uint32_t g = intData[1];

	std::cout << "\te[1] = " << (uint32_t) e << '\n';
	std::cout << "\tf[4] = " << f << '\n';
	std::cout << "\tg[4] = " << g << '\n';
}

void printPageMiddle(std::vector<uint8_t>& data, int pageNo)
{
	std::cout << "Page " << pageNo << ":\n";

	std::cout << std::hex;
	for (int k = 0; k < 2; k++)
	{
		uint8_t* intData = &data[pageNo * PAGE_SIZE];
		std::cout << "\t";

		const int kSplit = 60;
		for (int i = 0; i < kSplit; i++)
		{
			if (i != 0)
			{
				std::cout << " ";
			}
			std::cout << std::setfill('0') << std::setw(2) << (int32_t) intData[i];
		}
		std::cout << "\n\t";

		for (int i = kSplit; i < kSplit + 61; i++)
		{
			if (i != kSplit)
			{
				std::cout << " ";
			}
			std::cout << std::setfill('0') << std::setw(2) << (int32_t) intData[i];
		}
		std::cout << "\n\n";
		std::cout << std::dec;
	}
}

int main(int argc, char** argv)
{
	const std::string fileName = "D:/Revers/Arduino/ATmega2560/FlashEdit/build/Debug/memory.bin";
	std::vector<uint8_t> data = readFile(fileName);

	if (data.size() != EXPECTED_SIZE)
	{
		std::cout << "Error: invalid data size: " << data.size() << std::endl;
		return -1;
	}
	printPage('X', data, 0);

	std::cout << std::endl;
	printPage('Y', data, 256);

	SimplePage first = getSimplePage(data, 0);
	SimplePage second = getSimplePage(data, 256);

	std::cout << std::endl;
	std::cout << "X_a - Y_a = " << (first.a - second.a) << std::endl;
	std::cout << "X_b - Y_b = " << (first.b - second.b) << std::endl;
	std::cout << "X_c - Y_c = " << (first.c - second.c) << std::endl;
	std::cout << "X_d - Y_d = " << (first.d - second.d) << std::endl;

	std::cout << std::endl;
	printPageShort(data, 1792);

	std::cout << std::endl;
	printPageMiddle(data, 512);
	return 0;
}
