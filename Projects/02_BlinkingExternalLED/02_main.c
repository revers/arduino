#include <avr/io.h>
#include <util/delay.h>

/**
 * Blink external LED connected on Digital pin 2.
 *
 * Related links:
 * - https://www.instructables.com/id/Program-Arduino-Uno-in-C-Language/
 */

// http://www.ladyada.net/learn/avr/fuses.html
// https://www.microchip.com/webdoc/AVRLibcReferenceManual/group__avr__fuse.html
FUSES = {
    .low = LFUSE_DEFAULT,
    .high = HFUSE_DEFAULT,
    .extended = EFUSE_DEFAULT
};

// delay 62.5ns on a 16MHz AtMega
#define NOP __asm__ __volatile__ ("nop\n\t")

int main (void)
{
	// set pin 4 of PORTE for output
	// Digital pin 2 on Arduino board is mapped to pin 4 on port E
	// https://www.arduino.cc/en/Hacking/PinMapping2560
	DDRE |= _BV(DDE4);

#if 1
	// Blinking

	while(1) {

		/* set pin 4 high to turn led on */
		PORTE |= _BV(PORTE4);
		_delay_ms(500);

		/* set pin 4 low to turn led off */
		PORTE &= ~_BV(PORTE4);
		_delay_ms(500);
	}
#else
	// Constant lighting

	/* set pin 4 high to turn led on */
	PORTE |= _BV(PORTE4);

	while(1) {
		NOP;
	}
#endif
}
