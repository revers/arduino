#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

/**
 * Reading Analog input form Analog pin 0 (PF0/ADC0) and displaying its 8 MSB
 * on 8 LED diodes (on Digital Ports 22-29 + input interrupt on Digital pin 21 (PD0/INT0))
 *
 * Related links:
 * - http://maxembedded.com/2011/06/the-adc-of-the-avr/
 * - https://www.arduino.cc/en/Tutorial/AnalogReadSerial | schematics for potentiometer connection
 * - http://www.avr-tutorials.com/adc/utilizing-avr-adc-interrupt-feature | Interrupt based
 */

// http://www.ladyada.net/learn/avr/fuses.html
// https://www.microchip.com/webdoc/AVRLibcReferenceManual/group__avr__fuse.html
FUSES = {
    .low = LFUSE_DEFAULT,
    .high = HFUSE_DEFAULT,
    .extended = EFUSE_DEFAULT
};

/*
 * https://www.arduino.cc/en/Hacking/PinMapping2560
 * PA0 | Digital pin 22
 * PA1 | Digital pin 23
 * PA2 | Digital pin 24
 * PA3 | Digital pin 25
 * PA4 | Digital pin 26
 * PA5 | Digital pin 27
 * PA6 | Digital pin 28
 * PA7 | Digital pin 29
 *
 * PF0/ADC0 | Analog pin 0
 */

#define USE_INTERRUPT

#ifdef USE_INTERRUPT
/*ADC Conversion Complete Interrupt Service Routine (ISR)*/
ISR(ADC_vect)
{
	PORTA = (uint8_t) (ADC >> 2); // read value from ADC and save it to the LED register
	ADCSRA |= _BV(ADSC); // Start Conversion
}
#else
// read adc value
uint16_t adc_read()
{
    // start single conversion
    // write '1' to ADSC
    ADCSRA |= _BV(ADSC);

    // wait for conversion to complete
    // ADSC becomes '0' again
    // till then, run loop continuously
    while(ADCSRA & _BV(ADSC));

    return (ADC);
}
#endif

void adc_init()
{
    // AREF = AVcc, use ADC0 (MUX4..0 = 0x00000b)
    ADMUX = _BV(REFS0);

    // ADC Enable and prescaler of 128
    // 16000000Hz/128 = 125000Hz
    // (16Mhz/128 = 125kHz)
    ADCSRA = _BV(ADEN) | _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0);

#ifdef USE_INTERRUPT
    ADCSRA |= _BV(ADIE); // ADC Interrupt Enable
#endif
}

int main(void)
{
	// Set all 8 pins on port A for output
	DDRA |= 0xFF;

	// Configure PortF as input. PF0 is ADC0 input
	DDRF = 0x00;

	adc_init();

#ifdef USE_INTERRUPT
	sei();				 // Enable Global Interrupts
	ADCSRA |= _BV(ADSC); // Start Conversion
	while (1);
#else
	uint16_t adcValue;

	while (1) {
		adcValue = adc_read();
		PORTA = (uint8_t) (adcValue >> 2);
		_delay_ms(50);
	}
#endif
}
