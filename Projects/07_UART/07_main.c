// http://homepage.hispeed.ch/peterfleury/avr-software.html

/*************************************************************************
Title:    Example program for the Interrupt controlled UART library
Author:   Peter Fleury <pfleury@gmx.ch>   http://tinyurl.com/peterfleury
File:     $Id: test_uart.c,v 1.7 2015/01/31 17:46:31 peter Exp $
Software: AVR-GCC 4.x
Hardware: AVR with built-in UART/USART

DESCRIPTION:
          This example shows how to use the UART library uart.c

*************************************************************************/
#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

#include "uart.h"


/* define CPU frequency in Hz in Makefile */
#ifndef F_CPU
#error "F_CPU undefined, please define CPU frequency in Hz in Makefile"
#endif

/* Define UART buad rate here */
#define UART_BAUD_RATE      9600


// http://www.ladyada.net/learn/avr/fuses.html
// https://www.microchip.com/webdoc/AVRLibcReferenceManual/group__avr__fuse.html
FUSES = {
    .low = LFUSE_DEFAULT,
    .high = HFUSE_DEFAULT,
    .extended = EFUSE_DEFAULT
};

#define NL "\r\n"
#define CMD_BUF_SIZE 32

int main(void)
{
    char buffer[7];
    int  num=134;


    /*
     *  Initialize UART library, pass baudrate and AVR cpu clock
     *  with the macro
     *  UART_BAUD_SELECT() (normal speed mode )
     *  or
     *  UART_BAUD_SELECT_DOUBLE_SPEED() ( double speed mode)
	 */
	uart_init(UART_BAUD_SELECT(UART_BAUD_RATE, F_CPU));

    /*
     * now enable interrupt, since UART library is interrupt controlled
     */
    sei();

    /*
     *  Transmit string to UART
     *  The string is buffered by the uart library in a circular buffer
     *  and one character at a time is transmitted to the UART using interrupts.
     *  uart_puts() blocks if it can not write the whole string to the circular
     *  buffer
     */
    uart_puts("String stored in SRAM"NL);

    /*
     * Transmit string from program memory to UART
     */
    uart_puts_P("String stored in FLASH"NL);


    /*
     * Use standard avr-libc functions to convert numbers into string
     * before transmitting via UART
     */
    itoa( num, buffer, 10);   // convert interger into string (decimal format)
    uart_puts(buffer);        // and transmit string to UART

    unsigned int ret;
    char c;
    char cmdBuffer[CMD_BUF_SIZE];
    int cmdIndex = 0;

    /*
     * Transmit single character to UART
     */
    uart_puts(NL"> ");

	for (;;)
	{
		/*
		 * Get received character from ringbuffer
		 * uart_getc() returns in the lower byte the received character and
		 * in the higher byte (bitmask) the last receive error
		 * UART_NO_DATA is returned when no data is available.
		 *
		 */
		ret = uart_getc();

		if ((ret & UART_NO_DATA) == 0)
		{
			/*
			 * new data available from UART
			 * check for Frame or Overrun error
			 */
			if (ret & UART_FRAME_ERROR)
			{
				/* Framing Error detected, i.e no stop bit detected */
				uart_puts_P("UART Frame Error: ");
			}
			if (ret & UART_OVERRUN_ERROR)
			{
				/*
				 * Overrun, a character already present in the UART UDR register was
				 * not read by the interrupt handler before the next character arrived,
				 * one or more received characters have been dropped
				 */
				uart_puts_P("UART Overrun Error: ");
			}
			if (ret & UART_BUFFER_OVERFLOW)
			{
				/*
				 * We are not reading the receive buffer fast enough,
				 * one or more received character have been dropped
				 */
				uart_puts_P("Buffer overflow error: ");
			}
			c = (char) ret;

			if (c == '\r')
			{
				cmdBuffer[cmdIndex] = '\0';
				uart_putc(c);
			}
			else if (c == '\n')
			{
				if (cmdIndex > 0)
				{
					uart_puts("\nCommand: ");
					uart_puts(&cmdBuffer[0]);
					uart_putc('\r');
					cmdIndex = 0;
				}
				uart_puts("\n> ");
			}
			else
			{
				if (cmdIndex < CMD_BUF_SIZE - 2)
				{
					cmdBuffer[cmdIndex++] = c;
				}
				uart_putc(c);
			}
		}
	}

}
