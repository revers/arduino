#include <stdlib.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

/**
 * SPI Slave on ATmega328P (Arduiono Uno)
 *
 * Communication between 2 Arduinos using SPI bus.
 * Based on http://gammon.com.au/forum/?id=10892&reply=2#reply2
 *
 * Good description of SPI:
 * - http://maxembedded.com/2013/11/serial-peripheral-interface-spi-basics/
 * - http://maxembedded.com/2013/11/the-spi-of-the-avr/
 */

// http://www.ladyada.net/learn/avr/fuses.html
// https://www.microchip.com/webdoc/AVRLibcReferenceManual/group__avr__fuse.html
FUSES = {
    .low = LFUSE_DEFAULT,
    .high = HFUSE_DEFAULT,
    .extended = EFUSE_DEFAULT
};

// Settings for ATmega328P (Uno)
// https://www.arduino.cc/en/Hacking/PinMapping168
#define SPI_PORT_IN    PINB     /* This port should be used for reading if Data Direction is input */
#define SPI_PORT_OUT   PORTB    /* This port should be used for writing if Data Direction is output */
#define SPI_DDR        DDRB     /* Data Direction Register */
#define SPI_SS         PORTB2   /* Chip Select / Slave Select */
#define SPI_SCK        PORTB5
#define SPI_MOSI       PORTB3
#define SPI_MISO       PORTB4

// what to do with incoming data
volatile uint8_t command = 0;

void setupSPI()
{
  // MISO is output, the rest pins are input
  SPI_DDR = _BV(SPI_MISO);

  // Enable SPI in Slave
  SPCR = _BV(SPE);

  // turn on interrupts
  SPCR |= _BV(SPIE);
}

// SPI interrupt routine
ISR (SPI_STC_vect)
{
  uint8_t c = SPDR;

  switch (command)
  {
  // no command? then this is the command
  case 0:
    command = c;
    SPDR = 0;
    break;

  // add to incoming byte, return result
  case 'a':
    SPDR = c + 15;  // add 15
    break;

  // subtract from incoming byte, return result
  case 's':
    SPDR = c - 8;  // subtract 8
    break;
  }
}

void loop (void)
{
  if (SPI_PORT_IN & _BV(SPI_SS))
  {
    command = 0;
  }
}

int main()
{
    setupSPI();
    // Enable interrupts
	sei();

    while (1)
    {
    	loop();
    }
    return 0;
}
