#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

/**
 * Lighting 8 LED diodes on Digital Ports 22-29 + input interrupt on Digital pin 21 (PD0/INT0)
 *
 * Related links:
 * - http://www.avr-tutorials.com/interrupts/The-AVR-8-Bits-Microcontrollers-External-Interrupts
 * - http://www.avr-tutorials.com/interrupts/avr-external-interrupt-c-programming
 */

// http://www.ladyada.net/learn/avr/fuses.html
// https://www.microchip.com/webdoc/AVRLibcReferenceManual/group__avr__fuse.html
FUSES = {
    .low = LFUSE_DEFAULT,
    .high = HFUSE_DEFAULT,
    .extended = EFUSE_DEFAULT
};

#define DELAY_MS 200

//Interrupt Service Routine for INT0
ISR(INT0_vect)
{
	unsigned char i, temp;

	_delay_ms(500); // Software debouncing control

	temp = PORTA;	// Save current value on DataPort

	/* This for loop blink LEDs on Dataport 5 times*/
	for (i = 0; i < 5; i++) {
		PORTA = 0x00;
		_delay_ms(500);
		PORTA = 0xFF;
		_delay_ms(500);
	}
	PORTA = temp;	//Restore old value to DataPort
}


/*
 * https://www.arduino.cc/en/Hacking/PinMapping2560
 * PA0 | Digital pin 22
 * PA1 | Digital pin 23
 * PA2 | Digital pin 24
 * PA3 | Digital pin 25
 * PA4 | Digital pin 26
 * PA5 | Digital pin 27
 * PA6 | Digital pin 28
 * PA7 | Digital pin 29
 *
 * PD0/INT0 | Digital pin 21
 */
int main(void)
{
	cli(); // Disable Global Interrupts

	// Set all 8 pins on port A for output
	DDRA |= 0xFF;

	DDRD = _BV(PD0);  // Set PD0 as input (Using for interrupt INT0)
	PORTD = _BV(PD0); // Enable PD0 pull-up resistor

	EIMSK = _BV(INT0); // Enable INT0
	EICRA = _BV(ISC01) | _BV(ISC00); // Trigger INT0 on rising edge

	sei();				// Enable Global Interrupt
	int i;

	while(1) {
		// light on
		for (i = 0; i < 8; i++) {
			PORTA |= _BV(i);
			_delay_ms(DELAY_MS);
		}

		// light off
		for (i = 8; i >= 0; i--) {
			PORTA &= ~_BV(i);
			_delay_ms(DELAY_MS);
		}
	}
}
