#include <stdlib.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "uart.h"

/**
 * SPI Master on ATmega2560 (Arduino Mega)
 *
 * Communication between 2 Arduinos using SPI bus.
 * Based on http://gammon.com.au/forum/?id=10892&reply=2#reply2
 *
 * Good description of SPI:
 * - http://maxembedded.com/2013/11/serial-peripheral-interface-spi-basics/
 * - http://maxembedded.com/2013/11/the-spi-of-the-avr/
 */

// http://www.ladyada.net/learn/avr/fuses.html
// https://www.microchip.com/webdoc/AVRLibcReferenceManual/group__avr__fuse.html
FUSES = {
    .low = LFUSE_DEFAULT,
    .high = HFUSE_DEFAULT,
    .extended = EFUSE_DEFAULT
};

// Settings for ATmega2560
// https://www.arduino.cc/en/Hacking/PinMapping2560
#define SPI_PORT_IN    PINB     /* This port should be used for reading if Data Direction is input */
#define SPI_PORT_OUT   PORTB    /* This port should be used for writing if Data Direction is output */
#define SPI_DDR        DDRB     /* Data Direction Register */
#define SPI_SS         PB0      /* Chip Select / Slave Select */
#define SPI_SCK        PB1
#define SPI_MOSI       PB2
#define SPI_MISO       PB3

/* Define UART buad rate here */
#define UART_BAUD_RATE      115200
#define NL                  "\r\n"

uint8_t SPI_WriteRead(uint8_t dataout);

void setupUART()
{
	uart_init(UART_BAUD_SELECT(UART_BAUD_RATE, F_CPU));
	/*
	 * now enable interrupt, since UART library is interrupt controlled
	 */
	sei();
	uart_puts("Start"NL);
}

void setupSPI()
{
	// Put SCK, MOSI, SS pins into output mode
	SPI_DDR = _BV(SPI_SCK) | _BV(SPI_MOSI) | _BV(SPI_SS);

	// Put CS/SS into HIGH state, other pins (e.g.: SCK, MOSI) into LOW state.
	SPI_PORT_OUT = _BV(SPI_SS);

	// Enable SPI, Master
	SPCR = _BV(SPE) | _BV(MSTR);

	{ // Clock divider: 8
		SPSR |= _BV(SPI2X);  // 1
		SPCR &= ~_BV(SPR1);  // 0
		SPCR |= _BV(SPR0);   // 1
	}
}

uint8_t SPI_WriteRead(uint8_t dataout)
{
	// Start transmission (MOSI)
	SPDR = dataout;
	// Wait for transmission complete
	while (!(SPSR & _BV(SPIF)))
		;
	// Return Serial In Value (MISO)
	return SPDR;
}

uint8_t transferAndWait(const uint8_t what)
{
  uint8_t a = SPI_WriteRead(what);
  _delay_us(20);
  return a;
}

void printString(const char* msg)
{
	uart_puts(msg);
	uart_puts(NL);
}

void printNumber(uint8_t val)
{
	char buffer[7];

	itoa(val, buffer, 10);
	uart_puts(buffer);
	uart_puts(NL);
}

void loop(void)
{
  uint8_t a, b, c, d;

  // enable Slave Select
  // digitalWrite(SS, LOW);
  SPI_PORT_OUT &= ~_BV(SPI_SS);

  transferAndWait('a');  // add command

  transferAndWait(10);
  a = transferAndWait(17);
  b = transferAndWait(33);
  c = transferAndWait(42);
  d = transferAndWait(0);

  // disable Slave Select
  //digitalWrite(SS, HIGH);
  SPI_PORT_OUT |= _BV(SPI_SS);

  printString("Adding results:");
  printNumber(a);
  printNumber(b);
  printNumber(c);
  printNumber(d);

  // enable Slave Select
//  digitalWrite(SS, LOW);
  SPI_PORT_OUT &= ~_BV(SPI_SS);

  transferAndWait('s');  // subtract command

  transferAndWait(10);
  a = transferAndWait(17);
  b = transferAndWait(33);
  c = transferAndWait(42);
  d = transferAndWait(0);

  // disable Slave Select
  // digitalWrite(SS, HIGH);
  SPI_PORT_OUT |= _BV(SPI_SS);

  printString("Subtracting results:");
  printNumber(a);
  printNumber(b);
  printNumber(c);
  printNumber(d);

  _delay_ms(1000);  // 1 second delay
}  // end of loop

int main()
{
	setupUART();
    setupSPI();

    while (1)
    {
    	loop();
    }
    return 0;
}
