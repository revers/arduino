#include <avr/io.h>
#include <util/delay.h>

/**
 * Lighting 8 LED diodes on Digital Ports 22-29
 *
 * Related links:
 * - https://www.instructables.com/id/Program-Arduino-Uno-in-C-Language/
 */

// http://www.ladyada.net/learn/avr/fuses.html
// https://www.microchip.com/webdoc/AVRLibcReferenceManual/group__avr__fuse.html
FUSES = {
    .low = LFUSE_DEFAULT,
    .high = HFUSE_DEFAULT,
    .extended = EFUSE_DEFAULT
};

#define DELAY_MS 200

/*
 * https://www.arduino.cc/en/Hacking/PinMapping2560
 * PA0 | Digital pin 22
 * PA1 | Digital pin 23
 * PA2 | Digital pin 24
 * PA3 | Digital pin 25
 * PA4 | Digital pin 26
 * PA5 | Digital pin 27
 * PA6 | Digital pin 28
 * PA7 | Digital pin 29
 */
int main (void)
{
	// Set all 8 pins on port A for output
	DDRA |= 0xFF;
	int i;

	while(1) {
		// light on
		for (i = 0; i < 8; i++) {
			PORTA |= _BV(i);
			_delay_ms(DELAY_MS);
		}

		// light off
		for (i = 8; i >= 0; i--) {
			PORTA &= ~_BV(i);
			_delay_ms(DELAY_MS);
		}
	}
}
