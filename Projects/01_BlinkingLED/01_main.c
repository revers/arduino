#include <avr/io.h>
#include <util/delay.h>

/**
 * Blink built-in LED diode.
 *
 * Related links:
 * - https://www.instructables.com/id/Program-Arduino-Uno-in-C-Language/
 */

// http://www.ladyada.net/learn/avr/fuses.html
// https://www.microchip.com/webdoc/AVRLibcReferenceManual/group__avr__fuse.html
FUSES = {
    .low = LFUSE_DEFAULT,
    .high = HFUSE_DEFAULT,
    .extended = EFUSE_DEFAULT
};

int main (void)
{
	/* set pin 7 of PORTB for output*/
	// Digital pin 13 (LED_BUILTIN) on Arduino board is mapped to pin 7 on port B
	// https://www.arduino.cc/en/Hacking/PinMapping2560
	DDRB |= _BV(DDB7);

	while(1) {

		/* set pin 7 high to turn led on */
		PORTB |= _BV(PORTB7);
		_delay_ms(500);

		/* set pin 7 low to turn led off */
		PORTB &= ~_BV(PORTB7);
		_delay_ms(500);
	}
}
