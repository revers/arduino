// Setup of EEPROM: "Example of I2C to access EEPROM" on http://gammon.com.au/i2c
//
// Code based on
//   http://homepage.hispeed.ch/peterfleury/avr-software.html
// but with fixes (addressing, frequency) based on:
//   https://embedds.com/programming-avr-i2c-interface/

/****************************************************************************
 Title:    Access serial EEPROM 24C02 using I2C interace
 Author:   Peter Fleury <pfleury@gmx.ch>
 File:     $Id: test_i2cmaster.c,v 1.3 2015/09/16 09:29:24 peter Exp $
 Software: AVR-GCC 4.x
 Hardware: any AVR device can be used when using i2cmaster.S or any
 AVR device with hardware TWI interface when using twimaster.c

 Description:
 This example shows how the I2C/TWI library i2cmaster.S or twimaster.c
 can be used to access a serial eeprom.

 *****************************************************************************/
#include <stdlib.h>
#include <avr/interrupt.h>
#include <avr/io.h>

#include "i2cmaster.h"
#include "uart.h"

#define Dev24C02  (0x50 << 1)      // device address of EEPROM 24C02, see datasheet

/* define CPU frequency in Hz in Makefile */
#ifndef F_CPU
#error "F_CPU undefined, please define CPU frequency in Hz in Makefile"
#endif

/* Define UART buad rate here */
#define UART_BAUD_RATE      9600

// http://www.ladyada.net/learn/avr/fuses.html
// https://www.microchip.com/webdoc/AVRLibcReferenceManual/group__avr__fuse.html
FUSES = {
		.low = LFUSE_DEFAULT,
		.high = HFUSE_DEFAULT,
		.extended = EFUSE_DEFAULT
};

#define NL "\r\n"

void println(unsigned char ret)
{
	char buffer[7];
	itoa(ret, buffer, 10);   // convert interger into string (decimal format)
	uart_puts(buffer);        // and transmit string to UART
	uart_puts(NL);
}

void printError(uint8_t err, const char* msg, int line)
{
	char buffer[7];

	uart_puts(msg);
	uart_puts(" | err: ");
	itoa(err, buffer, 10);
	uart_puts(buffer);

	uart_puts(" | line: ");
	itoa(line, buffer, 10);
	uart_puts(buffer);

	uart_puts(NL);
}

#define LOG_LINE __LINE__
#define CHECK_ERR(err, msg) do { if (err) { printError(err, msg, LOG_LINE); } } while(0)

int main(void)
{
	uint8_t ret;
	uint16_t addr = 0x1234;
	uint8_t val = 123;

	uart_init(UART_BAUD_SELECT(UART_BAUD_RATE, F_CPU));
	/*
	 * now enable interrupt, since UART library is interrupt controlled
	 */
	sei();
	uart_puts("Start"NL);

	i2c_init();                                // init I2C interface

	/* write 0x75 to eeprom address 0x05 (Byte Write) */
	ret = i2c_start(Dev24C02 + I2C_WRITE);       // set device address and write mode
	if (ret) {
		/* failed to issue start condition, possibly no device found */
		i2c_stop();

		CHECK_ERR(ret, "i2c_start");
	} else {
		// Write:
		{
			/* Wirte address */
			ret = i2c_write((uint8_t) (addr >> 8));
			CHECK_ERR(ret, "i2c_write");
			ret = i2c_write((uint8_t) addr);
			CHECK_ERR(ret, "i2c_write");

			/* Write value */
			ret = i2c_write(val);
			CHECK_ERR(ret, "i2c_write");
			i2c_stop(); // set stop conditon = release bus
		}
		// Read:
		{
			i2c_start_wait(Dev24C02 + I2C_WRITE);     // set device address and write mode
			/* Wirte address */
			ret = i2c_write((uint8_t) (addr >> 8));
			CHECK_ERR(ret, "i2c_write");
			ret = i2c_write((uint8_t) addr);
			CHECK_ERR(ret, "i2c_write");

			/* Read value */
			ret = i2c_rep_start(Dev24C02 + I2C_READ);       // set device address and read mode
			CHECK_ERR(ret, "i2c_rep_start");
			ret = i2c_readNak();                    // read one byte
			i2c_stop();

			uart_puts("Read: ");
			println(ret);
		}
	}

	unsigned int uartRet;
	for (;;) {
		uartRet = uart_getc();

		if ((uartRet & UART_NO_DATA) == 0)
				{
			uart_putc((unsigned char) uartRet);
		}
	}
}
