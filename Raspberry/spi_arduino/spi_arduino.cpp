// g++ -Wall -o spi_arduino spi_arduino.cpp -L. -I. -lbcm2835 -lwiringPi && sudo ./spi_arduino

#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <errno.h>

#include <wiringPi.h>
#include "bcm2835.h"


uint8_t transferAndWait (const uint8_t what)
{
  uint8_t a = bcm2835_spi_transfer(what);
  delayMicroseconds (20);
  return a;
} // end of transferAndWait

#define SS 0

int main(int argc, char** argv) 
{
	if (!bcm2835_init())
	{
      printf("bcm2835_init failed. Are you running as root??\n");
      return -1;
    }

    if (!bcm2835_spi_begin())
    {
      printf("bcm2835_spi_begin failed. Are you running as root??\n");
      return -1;
    }
    bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);      // The default
    bcm2835_spi_setDataMode(BCM2835_SPI_MODE1);                   // The default
    bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_64); // The default
//	bcm2835_spi_set_speed_hz(5000000);
    bcm2835_spi_chipSelect(BCM2835_SPI_CS0);                      // The default
    bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW);      // the default

    // ===================================================

	wiringPiSetup();
	pinMode(SS, OUTPUT);

	uint8_t a, b, c, d;
  
  // enable Slave Select
  digitalWrite(SS, LOW);    

  uint8_t x = transferAndWait ('a');  // add command
  uint8_t y = transferAndWait (10);
  a = transferAndWait (17);
  b = transferAndWait (33);
  c = transferAndWait (42);
  d = transferAndWait (0);

  // disable Slave Select
  digitalWrite(SS, HIGH);

  printf("Adding results (x: %d, y: %d):\n%d\n%d\n%d\n%d\n", (int) x, (int) y, (int) a, (int) b, (int) c, (int) d);

  
  // enable Slave Select
  digitalWrite(SS, LOW);   

  transferAndWait ('s');  // subtract command
  transferAndWait (10);
  a = transferAndWait (17);
  b = transferAndWait (33);
  c = transferAndWait (42);
  d = transferAndWait (0);

  // disable Slave Select
  digitalWrite(SS, HIGH);

  printf("Subtractic results:\n%d\n%d\n%d\n%d\n", (int) a, (int) b, (int) c, (int) d);
  return 0;
}
