#include <QDesktopWidget>
#include <QApplication>

#include "FlashEditWindow.hpp"

namespace
{

void center(QWidget& widget)
{

	int width = widget.width();
	int height = widget.height();

	QDesktopWidget* desktop = QApplication::desktop();

	int screenWidth = desktop->width();
	int screenHeight = desktop->height();

	int x = (screenWidth - width) / 2;
	int y = (screenHeight - height) / 2;

	widget.setGeometry(x, y, width, height);
}

} /* anonymous namespace */

int main(int argc, char** argv)
{
	QApplication app(argc, argv);
	app.setApplicationName("FlashEdit");

	fe::FlashEditWindow window;
	window.show();
	center(window);

	return app.exec();
}
