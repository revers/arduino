#ifndef FLASHPROGRAMMER_HPP_
#define FLASHPROGRAMMER_HPP_

#include <memory>
#include <string>
#include <vector>

#include "IFlashProgrammerListener.hpp"

namespace serial
{
class Serial;
}

namespace fe
{

struct PortInfo
{
	std::string port;
	std::string description;

	PortInfo(const std::string& port, const std::string& description) :
			port(port), description(description)
	{
	}
};

struct LinesAndShots
{
	uint32_t lines = 0;
	uint32_t shots = 0;

	LinesAndShots(uint32_t lines, uint32_t shots) :
			lines(lines), shots(shots)
	{
	}
};

class FlashProgrammer
{
public:
	public:
	FlashProgrammer(IFlashProgrammerListener* listener);
	virtual ~FlashProgrammer();

	bool connect(const std::string& port);
	bool isConnected();

	bool readAll(std::vector<uint8_t>& result);
	bool writeAll(const std::vector<uint8_t>& data);
	bool ereaseAll();

	void saveTxtAndBinFiles(const std::string& fileNameWithoutExt, const std::vector<uint8_t>& data);
	void saveAnalysisFile(const std::string& outFile, const std::vector<uint8_t>& data);

	std::string getPreferredPort();

	static std::vector<PortInfo> listPorts();
	static LinesAndShots getLinesAndShots(const std::vector<uint8_t>& data);
	static void setLinesAndShots(std::vector<uint8_t>& data, uint32_t lines, uint32_t shots);

private:
	void logHost(const std::string& msg);
	void logHostError(const std::string& msg);
	void logDevice(const std::string& msg);

	void sendCommand(const std::string& command, bool printCommand = true);
	bool writePage(int pageNum, const uint8_t* data, int dataSize);

private:
	class TimeReporter;

	IFlashProgrammerListener* listener = nullptr;
	std::shared_ptr<serial::Serial> serialConnection;
};

} /* namespace fe */

#endif /* FLASHPROGRAMMER_HPP_ */
