#include "FlashAnalyzer.hpp"

#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>

#include "FlashDefs.h"

namespace
{

struct SimplePage
{
	int32_t a;
	int32_t b;
	int32_t c;
	int32_t d;
};

SimplePage getSimplePage(const std::vector<uint8_t>& data, int pageNo)
{
	uint32_t* intData = (uint32_t*) &data[pageNo * FE_PAGE_SIZE];
	uint32_t a = intData[0];
	uint32_t b = intData[1];
	uint32_t c = intData[2];
	uint8_t d = data[pageNo * FE_PAGE_SIZE + 3 * sizeof(uint32_t)];

	return
	{	(int32_t) a, (int32_t) b, (int32_t) c, (int32_t) d};
}

void printPage(std::ostream& out, char id, const std::vector<uint8_t>& data, int pageNo)
{
	out << "Page " << pageNo << ": ";

	for (int i = 0; i < 13; i++)
	{
		if (i != 0)
		{
			out << " ";
		}
		out << std::setfill('0') << std::setw(2) << std::hex
				<< ((uint32_t) data[pageNo * FE_PAGE_SIZE + i]);
	}
	out << std::dec;
	out << std::endl;

	uint32_t* intData = (uint32_t*) &data[pageNo * FE_PAGE_SIZE];
	uint32_t a = intData[0];
	uint32_t b = intData[1];
	uint32_t c = intData[2];
	uint8_t d = data[pageNo * FE_PAGE_SIZE + 3 * sizeof(uint32_t)];

	out << "\t" << id << "_a[4] = " << a << '\n';
	out << "\t" << id << "_b[4] = " << b << '\n';
	out << "\t" << id << "_c[4] = " << c << '\n';
	out << "\t" << id << "_d[1] = " << (int32_t) d << '\n';

	out << "\t" << id << "_c - " << id << "_b = " << (c - b) << "  ->  " << id << "_b + " << (c - b) << " = "
			<< id << "_c\n";
}

void printPageShort(std::ostream& out, const std::vector<uint8_t>& data, int pageNo)
{
	out << "Page " << pageNo << ": ";

	for (int i = 0; i < 9; i++)
	{
		if (i != 0)
		{
			out << " ";
		}
		out << std::setfill('0') << std::setw(2) << std::hex
				<< ((uint32_t) data[pageNo * FE_PAGE_SIZE + i]);
	}
	out << std::dec;
	out << std::endl;

	uint32_t* intData = (uint32_t*) &data[pageNo * FE_PAGE_SIZE + 1];

	uint8_t e = data[pageNo * FE_PAGE_SIZE];
	uint32_t f = intData[0];
	uint32_t g = intData[1];

	out << "\te[1] = " << (uint32_t) e << '\n';
	out << "\tf[4] = " << f << '\n';
	out << "\tg[4] = " << g << '\n';
}

void printPageMiddle(std::ostream& out, const std::vector<uint8_t>& data, int pageNo)
{
	out << "Page " << pageNo << ":\n";

	out << std::hex;
	for (int k = 0; k < 2; k++)
	{
		const uint8_t* intData = &data[pageNo * FE_PAGE_SIZE];
		out << "\t";

		const int kSplit = 60;
		for (int i = 0; i < kSplit; i++)
		{
			if (i != 0)
			{
				out << " ";
			}
			out << std::setfill('0') << std::setw(2) << (int32_t) intData[i];
		}
		out << "\n\t";

		for (int i = kSplit; i < kSplit + 61; i++)
		{
			if (i != kSplit)
			{
				out << " ";
			}
			out << std::setfill('0') << std::setw(2) << (int32_t) intData[i];
		}
		out << "\n\n";
		out << std::dec;
	}
}

} /* anonymous namespace */

namespace fe
{

namespace FlashAnalyzer
{

bool analyze(const std::vector<uint8_t>& data, std::ostream& out)
{
	if (data.size() != FE_TOTAL_MEM_SIZE)
	{
		out << "Error: invalid data size: " << data.size() << std::endl;
		return false;
	}
	printPage(out, 'X', data, 0);

	out << std::endl;
	printPage(out, 'Y', data, 256);

	SimplePage first = getSimplePage(data, FE_PAGE_LINES_AND_SHOTS_SECONDARY);
	SimplePage second = getSimplePage(data, FE_PAGE_LINES_AND_SHOTS_PRIMARY);

	out << std::endl;
	out << "X_a - Y_a = " << (first.a - second.a) << std::endl;
	out << "X_b - Y_b = " << (first.b - second.b) << std::endl;
	out << "X_c - Y_c = " << (first.c - second.c) << std::endl;
	out << "X_d - Y_d = " << (first.d - second.d) << std::endl;

	out << std::endl;
	printPageShort(out, data, FE_PAGE_SERIAL_NO_AND_LIMIT);

	out << std::endl;
	printPageMiddle(out, data, FE_PAGE_SETTINGS);
	out << std::endl;

	return true;
}

} /* namespace FlashAnalyzer */

} /* namespace fe */
