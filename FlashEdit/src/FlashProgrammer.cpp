#include "FlashProgrammer.hpp"

#include <chrono>
#include <condition_variable>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <mutex>
#include <sstream>
#include <thread>

#include "serial/serial.h"

#include "FlashDefs.h"
#include "FlashAnalyzer.hpp"

#define STATUS_PREFIX      '\x01'
#define STATUS_PREFIX_STR  "\x01"
#define END_OF_DATA        0x03
#define ESCAPE             0x1B

#define STATUS_MESSAGE   (STATUS_PREFIX_STR ":")
#define STATUS_READY     (STATUS_PREFIX_STR "READY")
#define STATUS_ERROR     (STATUS_PREFIX_STR "ERROR")
#define STATUS_DONE      (STATUS_PREFIX_STR "DONE")
#define STATUS_PROCEED   (STATUS_PREFIX_STR "GO")
#define STATUS_MESSAGE   (STATUS_PREFIX_STR ":")

#define SERIAL_SIGNATURE  "CH340"

namespace fe
{

class FlashProgrammer::TimeReporter
{
	FlashProgrammer& programmer;
	std::chrono::steady_clock::time_point startTime;

public:
	TimeReporter(FlashProgrammer& programmer) :
			programmer(programmer)
	{
		startTime = std::chrono::steady_clock::now();
	}

	~TimeReporter()
	{
		auto endTime = std::chrono::steady_clock::now();

		int64_t elapsedMs = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count();
		programmer.logHost("Executed in: " + formatDuration(elapsedMs) + "s");
	}

	std::string formatDuration(uint64_t ms)
	{
		std::ostringstream oss;

		oss << std::setw(2)
				<< std::setfill('0')
				<< (ms / 1000)
				<< "."
				<< std::setw(3)
				<< (ms % 1000);
		return oss.str();
	}
};

namespace
{

std::string repr(const std::string& line)
{
	std::ostringstream oss;

	for (char c : line)
	{
		if (c < 32 || c > 126)
		{
			oss << "\\x" << std::uppercase << std::setfill('0') << std::setw(2) << std::hex
					<< ((int) c);
		}
		else
		{
			oss << c;
		}
	}
	return oss.str();
}

std::string getLine(serial::Serial& serialConnection)
{
	std::string line = "";

	while (true)
	{
		std::string readBytes = serialConnection.read();

		for (char c : readBytes)
		{
			if (c == '\r')
			{
				continue;
			}
			if (c == '\n')
			{
				return line;
			}
			else
			{
				line += c;
			}
		}
	}
	return line;
}

struct BinLine
{
	bool isStatus = false;
	std::vector<uint8_t> data;

	std::string toString()
	{
		data.push_back(0);
		std::string result = (const char*) &data[0];
		data.pop_back();

		return result;
	}
};

BinLine getBinaryLine(serial::Serial& serialConnection)
{
	BinLine result;

	bool doEscape = false;

	while (true)
	{
		char c;
		size_t bytesRead = serialConnection.read((uint8_t*) &c, 1);

		if (bytesRead < 1)
		{
			continue;
		}
		if (!doEscape && c == '\x1B')
		{
			doEscape = true;
			continue;
		}
		if (!doEscape && c == '\n')
		{
			result.data.pop_back(); // because the last element is always useless \r
			return result;
		}
		else
		{
			if (result.data.empty() && !doEscape && c == STATUS_PREFIX)
			{
				// If the very first letter is STATUS_PREFIX then we received a status,
				// not a binary data.
				// Note that in binary data the status character is always escaped.
				result.isStatus = true;
			}

			doEscape = false;
			result.data.push_back((uint8_t) c);
		}
	}
	return BinLine();
}

} /* anonymous namespace */

FlashProgrammer::FlashProgrammer(IFlashProgrammerListener* listener) :
		listener(listener)
{
}

FlashProgrammer::~FlashProgrammer()
{
}

bool FlashProgrammer::connect(const std::string& port)
{
	if (isConnected())
	{
		return false;
	}
	TimeReporter timeReporter(*this);

	logHost("Trying to connect to port: " + port);
	logHost("Baud rate: " + std::to_string(FE_BAUD_RATE));

	try
	{
		serialConnection.reset(new serial::Serial(port, FE_BAUD_RATE, serial::Timeout::simpleTimeout(1000)));
	}
	catch (const std::exception& e)
	{
		logHostError("Failed to open connection for port '" + port + "': An exception was thrown: " + e.what());
		return false;
	}
	if (!serialConnection->isOpen())
	{
		serialConnection.reset();
		logHostError("Failed to open connection for port '" + port + "'");
		return false;
	}
	logHost("Port communication started. Verifying device...");

	struct ConnectionState
	{
		std::mutex mtx;
		std::condition_variable cv;
		bool canceled = false;
		bool deviceRecognized = false;
		std::weak_ptr<serial::Serial> serialConnectionWeak;

		ConnectionState(std::weak_ptr<serial::Serial> serialConnectionWeak) :
				serialConnectionWeak(serialConnectionWeak)
		{
		}
	};
	auto connState = std::make_shared<ConnectionState>(serialConnection);

	std::thread connThread([this, connState] ()
	{
		while (!connState->canceled)
		{
			if (auto serialConnection = connState->serialConnectionWeak.lock())
			try
			{
				std::string line = getLine(*serialConnection);

				if (line == STATUS_READY)
				{
					std::unique_lock<std::mutex> lck(connState->mtx);

					if (!connState->canceled)
					{
						logHost("Success");
						connState->deviceRecognized = true;
						connState->cv.notify_all();
					}
					break;
				}
				else if (line == STATUS_ERROR)
				{
					std::unique_lock<std::mutex> lck(connState->mtx);

					if (!connState->canceled)
					{
						logHost("Error");
					}
					break;
				}
				else
				{
					std::unique_lock<std::mutex> lck(connState->mtx);

					if (!connState->canceled)
					{
						logDevice(line);
					}
					else
					{
						break;
					}
				}
			}
			catch (const std::exception& ex)
			{
				logHostError(ex.what());
				break;
			}
			catch (...)
			{
				logHostError("Unknown exception");
				break;
			}
			else
			{
				// serial connection is not available
			break;
		}
	}
});

	std::unique_lock<std::mutex> lck(connState->mtx);
	const int kSleepSeconds = 5;

	if (connState->cv.wait_for(lck, std::chrono::seconds(kSleepSeconds)) == std::cv_status::timeout)
	{
		connState->canceled = true;
		serialConnection.reset();
		connThread.detach();

		logHostError("Device did not respond within time limit (" + std::to_string(kSleepSeconds) + "s)");

		return false;
	}
	if (connThread.joinable())
	{
		connThread.join();
	}
	logHost("Device verified successfully");

	return true;
}

bool FlashProgrammer::isConnected()
{
	return serialConnection != nullptr;
}

std::vector<PortInfo> FlashProgrammer::listPorts()
{
	std::vector<serial::PortInfo> devicesFound = serial::list_ports();

	std::vector<fe::PortInfo> result;
	result.reserve(devicesFound.size());

	for (serial::PortInfo portInfo : devicesFound)
	{
		result.emplace_back(portInfo.port, portInfo.description);
	}
	return result;
}

std::string FlashProgrammer::getPreferredPort()
{
	std::vector<serial::PortInfo> devicesFound = serial::list_ports();

	std::vector<std::string> matchedPorts;

	for (serial::PortInfo portInfo : devicesFound)
	{
		if (portInfo.description.find(SERIAL_SIGNATURE) != std::string::npos)
		{
			matchedPorts.push_back(portInfo.port);
		}
	}
	if (matchedPorts.size() == 1)
	{
		return matchedPorts[0];
	}
	if (matchedPorts.empty())
	{
		logHost("No suitable port found.");
		return "";
	}
	std::ostringstream oss;
	oss << "Multiple suitable ports found: ";

	int i = 0;
	for (const std::string& port : matchedPorts)
	{
		if (i != 0)
		{
			oss << ", ";
		}
		i++;
		oss << port;
	}
	logHost(oss.str());

	return "";
}

void FlashProgrammer::logHost(const std::string& msg)
{
	listener->flashMessage("  [Host] " + msg);
}

void FlashProgrammer::logHostError(const std::string& msg)
{
	logHost("ERROR: " + msg);
}

void FlashProgrammer::logDevice(const std::string& msg)
{
	std::string log = "[Device] ";

	if (msg.find(STATUS_MESSAGE) == 0)
	{
		log += repr(msg.substr(strlen(STATUS_MESSAGE)));
	}
	else
	{
		log += repr(msg);
	}
	listener->flashMessage(log);
}

void FlashProgrammer::sendCommand(const std::string& command, bool printCommand)
{
	if (printCommand)
	{
		logHost("Command: " + command);
	}
	serialConnection->write(command + ';');
}

bool FlashProgrammer::readAll(std::vector<uint8_t>& result)
try
{
	TimeReporter timeReporter(*this);

	result.clear();
	result.reserve(FE_TOTAL_MEM_SIZE);

	listener->flashProgress(0);
	sendCommand("read_all");

	while (true)
	{
		BinLine binLine = getBinaryLine(*serialConnection);

		if (binLine.isStatus)
		{
			std::string line = binLine.toString();

			if (line == STATUS_DONE)
			{
				if (result.size() == 0)
				{
					logHost("Error: Received 0 bytes.");
					return false;
				}
				if (result.size() != FE_TOTAL_MEM_SIZE)
				{
					logHost(
							"Error: Received bytes: " + std::to_string(result.size()) + ". Expected: "
									+ std::to_string(FE_TOTAL_MEM_SIZE));
					return false;
				}
				logHost("Read done");
				return true;
			}
			else if (line == STATUS_ERROR)
			{
				logHost("Error");
				return true;
			}
			else
			{
				logDevice(line);
			}
		}
		else
		{
			result.insert(result.end(), binLine.data.begin(), binLine.data.end());
			double prc = 100.0 * result.size() / FE_TOTAL_MEM_SIZE;

			listener->flashProgress(static_cast<int>(prc + 0.5));
		}
	}
	return false;
}
catch (const std::exception& ex)
{
	logHostError(ex.what());
	return false;
}
catch (...)
{
	logHostError("Unknown exception");
	return false;
}

bool FlashProgrammer::ereaseAll()
try
{
	TimeReporter timeReporter(*this);
	sendCommand("erase_all");

	while (true)
	{
		std::string line = getLine(*serialConnection);

		if (line == STATUS_DONE)
		{
			logHost("Done");
			return true;
		}
		else if (line == STATUS_ERROR)
		{
			logHost("Error");
			return false;
		}
		else
		{
			logDevice(line);
		}
	}
	return false;
}
catch (const std::exception& ex)
{
	logHostError(ex.what());
	return false;
}
catch (...)
{
	logHostError("Unknown exception");
	return false;
}

bool FlashProgrammer::writePage(int pageNum, const uint8_t* data, int dataSize)
try
{
	if (dataSize != FE_PAGE_SIZE)
	{
		logHost(
				"Error: Write Page: Data size is incorrect: " + std::to_string(dataSize) + ". Expected: "
						+ std::to_string(FE_PAGE_SIZE));
		return false;
	}
	std::vector<uint8_t> escapedData;

	for (int i = 0; i < dataSize; i++)
	{
		uint8_t c = data[i];

		if (c == (uint8_t) END_OF_DATA || c == (uint8_t) ESCAPE)
		{
			escapedData.push_back(ESCAPE);
		}
		escapedData.push_back(c);
	}
	escapedData.push_back(END_OF_DATA);
	sendCommand("write_page:" + std::to_string(pageNum), false);

	while (true)
	{
		std::string line = getLine(*serialConnection);

		if (line == STATUS_PROCEED)
		{
			serialConnection->write(escapedData);
		}
		else if (line == STATUS_DONE)
		{
			return true;
		}
		else if (line == STATUS_ERROR)
		{
			logHost("Error");
			return false;
		}
		else
		{
			if (line.find("Write page") != 0)
			{
				logDevice(line);
			}
		}
	}
	return false;
}
catch (const std::exception& ex)
{
	logHostError(ex.what());
	return false;
}
catch (...)
{
	logHostError("Unknown exception");
	return false;
}

bool FlashProgrammer::writeAll(const std::vector<uint8_t>& data)
try
{
	if (data.size() != FE_TOTAL_MEM_SIZE)
	{
		logHost(
				"Error: Data size is incorrect: " + std::to_string(data.size()) + ". Expected: "
						+ std::to_string(FE_TOTAL_MEM_SIZE));
		return false;
	}
	listener->flashProgress(0);

	if (!ereaseAll())
	{
		return false;
	}
	TimeReporter timeReporter(*this);

	logHost("Starting upload...");

	for (int i = 0; i < (int) data.size(); i += FE_PAGE_SIZE)
	{
		int pageNum = i / FE_PAGE_SIZE;

		if (!writePage(pageNum, &data[i], FE_PAGE_SIZE))
		{
			return false;
		}
		int bytes = i + FE_PAGE_SIZE;
		double prc = 100.0 * bytes / FE_TOTAL_MEM_SIZE;

		listener->flashProgress((int) (prc + 0.5));
	}
	logHost("Write complete");

	return true;
}
catch (const std::exception& ex)
{
	logHostError(ex.what());
	return false;
}
catch (...)
{
	logHostError("Unknown exception");
	return false;
}

namespace
{
void saveHexData(const std::string& fileName, const std::vector<uint8_t>& data)
{
	std::fstream file(fileName, std::ios::out);

	for (int i = 0; i < (int) data.size(); i++)
	{
		if (i != 0 && (i % FE_PAGE_SIZE) == 0)
		{
			file << '\n';
		}
		file << std::setfill('0') << std::setw(2) << std::hex << (int) data[i];
	}
	file << '\n';
}

void saveBinData(const std::string& fileName, const std::vector<uint8_t>& data)
{
	std::fstream file(fileName, std::ios::out | std::ios::binary);
	file.write((const char*) &data[0], data.size());
}
} /* anonymous namespace */

void FlashProgrammer::saveTxtAndBinFiles(const std::string& fileNameWithoutExt, const std::vector<uint8_t>& data)
{
	std::string outputTxtFileName = fileNameWithoutExt + ".txt";
	std::string outputBinFileName = fileNameWithoutExt + ".bin";

	logHost("Saving results to " + outputTxtFileName + ", " + outputBinFileName);

	saveBinData(outputBinFileName, data);
	saveHexData(outputTxtFileName, data);

	logHost("Files " + outputTxtFileName + ", " + outputBinFileName + " have been saved.");
}

LinesAndShots FlashProgrammer::getLinesAndShots(const std::vector<uint8_t>& data)
{
	const uint32_t* arr = (const uint32_t*) &data[FE_PAGE_LINES_AND_SHOTS_PRIMARY * FE_PAGE_SIZE];
	return
	{	arr[0], arr[1]};
}

void FlashProgrammer::setLinesAndShots(std::vector<uint8_t>& data, uint32_t lines, uint32_t shots)
{
	uint32_t* arr = (uint32_t*) &data[FE_PAGE_LINES_AND_SHOTS_PRIMARY * FE_PAGE_SIZE];
	arr[0] = lines;
	arr[1] = shots;
}

void FlashProgrammer::saveAnalysisFile(const std::string& outFile, const std::vector<uint8_t>& data)
{
	std::fstream file(outFile, std::ios::out);
	FlashAnalyzer::analyze(data, std::cout);

	if (FlashAnalyzer::analyze(data, file))
	{
		logHost("Analysis file has been saved: " + outFile);
	}
	else
	{
		logHostError("Failed to perform memory analysis");
	}
}

} /* namespace fe */
