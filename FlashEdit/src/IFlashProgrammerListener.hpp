#ifndef IFLASHPROGRAMMERLISTENER_HPP_
#define IFLASHPROGRAMMERLISTENER_HPP_

#include <string>

namespace fe
{

class IFlashProgrammerListener
{
public:
	virtual ~IFlashProgrammerListener()
	{
	}

	virtual void flashMessage(const std::string& msg) = 0;
	virtual void flashProgress(int percent) = 0;
};

} /* namespace fe */

#endif /* IFLASHPROGRAMMERLISTENER_HPP_ */
