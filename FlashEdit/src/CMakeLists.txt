cmake_minimum_required(VERSION 3.9)

project(FlashEdit)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} -O3")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS} -g -O0")

# This is needed to make the program a window application instead of a console application.
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mwindows")

if (NOT CMAKE_BUILD_TYPE MATCHES Debug)
    add_definitions(-DNDEBUG)
endif()

set(QT_QMAKE_EXECUTABLE "D:/qt64/Qt64-4.8.4/bin/qmake.exe")

find_package(Qt4 COMPONENTS QtCore QtGui REQUIRED)

include(${QT_USE_FILE})

include_directories(${CMAKE_BINARY_DIR})

set(FE_SRC_DIR ${PROJECT_SOURCE_DIR})

set(FE_QT_HEADERS    ${FE_SRC_DIR}/FlashEditWindow.hpp)
set(FE_QT_SOURCES    ${FE_SRC_DIR}/FlashEditWindow.cpp)
set(FE_QT_UIS        ${FE_SRC_DIR}/FlashEditWindow.ui)
set(FE_QT_RESOURCES  ${FE_SRC_DIR}/FlashEdit.qrc)

QT4_WRAP_CPP(FE_QT_MOC_CPP ${FE_QT_HEADERS})
QT4_WRAP_UI(FE_QT_UI_CPP ${FE_QT_UIS})
QT4_ADD_RESOURCES(FE_QT_RES_RCC ${FE_QT_RESOURCES})

set(FE_SERIAL_DIR ${FE_SRC_DIR}/serial)

set(FE_SERIAL_HEADERS 
    ${FE_SERIAL_DIR}/serial.h
    ${FE_SERIAL_DIR}/v8stdint.h
    ${FE_SERIAL_DIR}/win.h
)
set(FE_SERIAL_SOURCES
    ${FE_SERIAL_DIR}/list_ports_win.cc
    ${FE_SERIAL_DIR}/serial.cc
    ${FE_SERIAL_DIR}/win.cc
)

set(FE_RC_FILES  ${FE_SRC_DIR}/FlashEdit.rc)
set(FE_HEADERS   
    ${FE_SRC_DIR}/FlashDefs.h
    ${FE_SRC_DIR}/IFlashProgrammerListener.hpp
    ${FE_SRC_DIR}/FlashAnalyzer.hpp
    ${FE_SRC_DIR}/FlashProgrammer.hpp
)
set(FE_SOURCES   
    ${FE_SRC_DIR}/FlashEdit.cpp
    ${FE_SRC_DIR}/FlashAnalyzer.cpp
    ${FE_SRC_DIR}/FlashProgrammer.cpp
)


set(FE_ALL_SOURCES
    ${FE_HEADERS}
    ${FE_SOURCES}
    ${FE_SERIAL_HEADERS}
    ${FE_SERIAL_SOURCES}
	${FE_QT_HEADERS}
    ${FE_QT_SOURCES}
	${FE_QT_MOC_CPP}
	${FE_QT_UI_CPP}
	${FE_QT_RES_RCC}
	${FE_RC_FILES}
)

set(FE_LIBS 
    ${QT_LIBRARIES} 
    setupapi
)

add_executable(${PROJECT_NAME} ${FE_ALL_SOURCES})
target_link_libraries(${PROJECT_NAME} ${FE_LIBS})
