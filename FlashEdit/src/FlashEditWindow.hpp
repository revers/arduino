#ifndef FLASHEDITWINDOW_H_
#define FLASHEDITWINDOW_H_

#include <memory>
#include <string>
#include <vector>

#include <QString>
#include <QMainWindow>

#include "FlashProgrammer.hpp"

class QLabel;
class QProgressBar;

namespace Ui
{
class MainWindow;
}

namespace fe
{

class FlashEditWindow: public QMainWindow, public IFlashProgrammerListener
{
Q_OBJECT

	enum class State
	{
		NONE, DISCONNECTED, CONNECTING, CONNECTED, READING, READ_DONE, WRITING, WRITE_DONE
	};

public:
	FlashEditWindow();
	virtual ~FlashEditWindow();

private:
	void flashMessage(const std::string& msg) override;
	void flashProgress(int percent) override;

	void initPortComboBox();
	void initStatusBar();
	void setupActions();

	void setState(State state);

	void setStatusDisconnected();
	void setStatusConnected();

	void setAllEnabled(bool enabled);
	void setPageEditEnabled(bool enabled);
	void updatePageFields();
	void updateDataFromPageFields();
	void refreshState();

private slots:
	void connectAction();
	void readAction();
	void writeAction();
	void loadAction();

	void logMessageAction(const QString& msg);
	void progressMadeAction(int percent);

	void connectionEstablishedAction();
	void connectionFailedAction();

	void readSucceededAction();
	void readFailedAction();

	void writeSucceededAction();
	void writeFailedAction();

signals:
	void logMessage(const QString& msg);
	void progressMade(int percent);

	void connectionEstablished();
	void connectionFailed();

	void readSucceeded();
	void readFailed();

	void writeSucceeded();
	void writeFailed();

private:
	FlashProgrammer flashProgrammer;
	std::unique_ptr<Ui::MainWindow> ui;

	QLabel* statusConnectionL = nullptr;
	QLabel* statusL = nullptr;
	QProgressBar* progressBar = nullptr;
	State state = State::NONE;

	std::vector<uint8_t> data;
	bool memoryLoaded = false;
};

} /* namespace fe */

#endif /* FLASHEDITWINDOW_H_ */
