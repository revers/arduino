#include "FlashEditWindow.hpp"

#include <climits>
#include <fstream>
#include <iostream>
#include <sstream>
#include <thread>
#include <vector>

#include <QFile>
#include <QFileDialog>
#include <QFont>
#include <QLabel>
#include <QMessageBox>
#include <QProgressBar>
#include <QScrollBar>
#include <QSettings>

#include "ui_FlashEditWindow.h"
#include "FlashDefs.h"

#define FE_VERSION "0.1"

#ifdef NDEBUG
#define WINDOW_TITLE "FlashEdit " FE_VERSION
#else
#define WINDOW_TITLE "FlashEdit " FE_VERSION " (DEBUG)"
#endif

namespace
{
std::vector<uint8_t> readFile(const std::string& filename)
{
	std::ifstream file(filename, std::ios::binary);

	return std::vector<uint8_t>((std::istreambuf_iterator<char>(file)),
			std::istreambuf_iterator<char>());
}
} /* anonymous namespace */

namespace fe
{

void init8BitSpinBox(QSpinBox* spinBox)
{
	spinBox->setMinimum(0);
	spinBox->setMaximum(0xFF);
}

void init32BitSpinBox(QSpinBox* spinBox)
{
	spinBox->setMinimum(LONG_MIN);
	spinBox->setMaximum(LONG_MAX);
}

FlashEditWindow::FlashEditWindow() :
		flashProgrammer(this),
				ui(new Ui::MainWindow),
				data(FE_TOTAL_MEM_SIZE, 0)
{

	ui->setupUi(this);
	setWindowTitle(WINDOW_TITLE);

	QFont font("Monospace");
	font.setStyleHint(QFont::TypeWriter);
	ui->logsTE->setFont(font);

	init32BitSpinBox(ui->pageXValASB);
	init32BitSpinBox(ui->pageXValBSB);
	init32BitSpinBox(ui->pageXValCSB);
	init8BitSpinBox(ui->pageXValDSB);

	init32BitSpinBox(ui->pageYValASB);
	init32BitSpinBox(ui->pageYValBSB);
	init32BitSpinBox(ui->pageYValCSB);
	init8BitSpinBox(ui->pageYValDSB);

	init8BitSpinBox(ui->pageZValESB);
	init32BitSpinBox(ui->pageZValFSB);
	init32BitSpinBox(ui->pageZValGSB);

	initPortComboBox();
	initStatusBar();

	setupActions();

	setState(State::DISCONNECTED);

	resize(800, 700);
}

FlashEditWindow::~FlashEditWindow()
{
}

void FlashEditWindow::flashMessage(const std::string& msg)
{
	emit logMessage(QString(msg.c_str()));
}

void FlashEditWindow::flashProgress(int percent)
{
	emit progressMade(percent);
}

void FlashEditWindow::loadAction()
{
	const QString DEFAULT_DIR_KEY("default_dir");

	QSettings mySettings;

	QString selfilter = tr("Memory images (*.bin)");
	QString filename = QFileDialog::getOpenFileName(this, tr("Open file"),
			mySettings.value(DEFAULT_DIR_KEY).toString(),
			tr("Memory images (*.bin)"), &selfilter);

	if (filename.isEmpty())
	{
		return;
	}
	QDir currentDir;
	mySettings.setValue(DEFAULT_DIR_KEY, currentDir.absoluteFilePath(filename));

	std::vector<uint8_t> newData = readFile(filename.toAscii().data());

	if (newData.size() != FE_TOTAL_MEM_SIZE)
	{
		std::ostringstream oss;
		oss << "Invalid " << filename.toAscii().data() << " file size: " << newData.size() << ". Expected: "
				<< FE_TOTAL_MEM_SIZE;
		logMessage(QString(("  [Host] Error: " + oss.str()).c_str()));
		QMessageBox::critical(this, "Error", oss.str().c_str());
		return;
	}
	logMessage("  [Host] File " + filename + " loaded successfully");

	data = newData;
	updatePageFields();

	memoryLoaded = true;
	refreshState();
}

void FlashEditWindow::refreshState()
{
	setState(state);
}

void FlashEditWindow::logMessageAction(const QString& msg)
{
	ui->logsTE->append(msg);
	ui->logsTE->verticalScrollBar()->setValue(ui->logsTE->verticalScrollBar()->maximum());
}

void FlashEditWindow::progressMadeAction(int percent)
{
	progressBar->setValue(percent);
}

void FlashEditWindow::initPortComboBox()
{
	ui->portCB->addItem("Auto-detect", QString(""));
	std::vector<PortInfo> availablePorts = FlashProgrammer::listPorts();

	for (const PortInfo& portInfo : availablePorts)
	{
		ui->portCB->addItem(QString(portInfo.port.c_str()) + " / " + portInfo.description.c_str(),
				QString(portInfo.port.c_str()));
	}
}

void FlashEditWindow::initStatusBar()
{
	setStyleSheet("QStatusBar{border-top: 1px outset grey; padding: 4px;}");

	statusConnectionL = new QLabel(this);
	statusConnectionL->setStyleSheet("margin: 2px;");

	ui->statusBar->addPermanentWidget(statusConnectionL);

	statusL = new QLabel(this);
	statusL->setStyleSheet("margin: 2px;");

	ui->statusBar->addPermanentWidget(statusL);

	progressBar = new QProgressBar(this);
	progressBar->setMinimum(0);
	progressBar->setMaximum(100);
	progressBar->setTextVisible(false);
	progressBar->setStyleSheet("margin: 2px;");

	ui->statusBar->addPermanentWidget(progressBar, 1);
}

void FlashEditWindow::setStatusDisconnected()
{
	statusConnectionL->setText("<html><img src=':/images/disconnected.png'> Disconnected ");
	statusL->setText("<html> Please connect to the device ");
}

void FlashEditWindow::setStatusConnected()
{
	statusConnectionL->setText("<html><img src=':/images/connected.png'> Connected ");
	statusL->setText("<html> Please read memory ");
}

void FlashEditWindow::setupActions()
{
	connect(ui->connectB, SIGNAL(clicked()), this, SLOT(connectAction()));
	connect(ui->readB, SIGNAL(clicked()), this, SLOT(readAction()));
	connect(ui->writeB, SIGNAL(clicked()), this, SLOT(writeAction()));
	connect(ui->loadMemoryB, SIGNAL(clicked()), this, SLOT(loadAction()));

	connect(this, SIGNAL(logMessage(const QString&)), this, SLOT(logMessageAction(const QString&)),
			Qt::QueuedConnection);
	connect(this, SIGNAL(progressMade(int)), this, SLOT(progressMadeAction(int)), Qt::QueuedConnection);

	connect(this, SIGNAL(connectionEstablished()), this, SLOT(connectionEstablishedAction()), Qt::QueuedConnection);
	connect(this, SIGNAL(connectionFailed()), this, SLOT(connectionFailedAction()), Qt::QueuedConnection);

	connect(this, SIGNAL(readSucceeded()), this, SLOT(readSucceededAction()), Qt::QueuedConnection);
	connect(this, SIGNAL(readFailed()), this, SLOT(readFailedAction()), Qt::QueuedConnection);

	connect(this, SIGNAL(writeSucceeded()), this, SLOT(writeSucceededAction()), Qt::QueuedConnection);
	connect(this, SIGNAL(writeFailed()), this, SLOT(writeFailedAction()), Qt::QueuedConnection);
}

void FlashEditWindow::setState(State state)
{
	this->state = state;

	switch (state)
	{
	case State::NONE:
		case State::DISCONNECTED:
		setStatusDisconnected();

		setAllEnabled(false);
		ui->loadMemoryB->setEnabled(true);
		ui->portCB->setEnabled(true);
		ui->connectB->setEnabled(true);
		setPageEditEnabled(memoryLoaded);
		break;

	case State::CONNECTING:
		setAllEnabled(false);
		statusL->setText("<html> Connecting... ");
		break;

	case State::CONNECTED:
		setStatusConnected();

		setAllEnabled(false);
		ui->readB->setEnabled(true);
		ui->loadMemoryB->setEnabled(true);
		ui->writeB->setEnabled(memoryLoaded);
		setPageEditEnabled(memoryLoaded);
		break;

	case State::READING:
		setAllEnabled(false);
		statusL->setText("<html> Reading... ");
		break;

	case State::READ_DONE:
		statusL->setText("<html><img src=':/images/success.png'> Read done ");

		ui->portCB->setEnabled(false);
		ui->connectB->setEnabled(false);
		ui->readB->setEnabled(true);
		ui->writeB->setEnabled(true);
		ui->loadMemoryB->setEnabled(true);
		setPageEditEnabled(true);
		break;

	case State::WRITING:
		setAllEnabled(false);
		statusL->setText("<html> Writing... ");
		break;

	case State::WRITE_DONE:
		statusL->setText("<html><img src=':/images/success.png'> Write done ");

		ui->portCB->setEnabled(false);
		ui->connectB->setEnabled(false);
		ui->readB->setEnabled(true);
		ui->writeB->setEnabled(true);
		setPageEditEnabled(true);
		break;
	}
}

void FlashEditWindow::setAllEnabled(bool enabled)
{
	ui->portCB->setEnabled(enabled);
	ui->connectB->setEnabled(enabled);
	ui->readB->setEnabled(enabled);
	ui->writeB->setEnabled(enabled);

	ui->loadMemoryB->setEnabled(enabled);
	setPageEditEnabled(enabled);
}

void FlashEditWindow::setPageEditEnabled(bool enabled)
{
	ui->pageXValASB->setEnabled(enabled);
	ui->pageXValBSB->setEnabled(enabled);
	ui->pageXValCSB->setEnabled(enabled);
	ui->pageXValDSB->setEnabled(enabled);

	ui->pageYValASB->setEnabled(enabled);
	ui->pageYValBSB->setEnabled(enabled);
	ui->pageYValCSB->setEnabled(enabled);
	ui->pageYValDSB->setEnabled(enabled);

	ui->pageZValESB->setEnabled(enabled);
	ui->pageZValFSB->setEnabled(enabled);
	ui->pageZValGSB->setEnabled(enabled);
}

void FlashEditWindow::connectAction()
{
	QString qPort = ui->portCB->itemData(ui->portCB->currentIndex()).toString();

	std::string port = qPort.toStdString();

	if (port.empty())
	{
		port = flashProgrammer.getPreferredPort();

		if (port.empty())
		{
			QMessageBox::warning(this, "Warning",
					"Could not auto-detect the device port. You need to chose it manually.");
			return;
		}
	}
	setState(State::CONNECTING);

	std::thread connectionThread([this, port]()
	{
		if (flashProgrammer.connect(port))
		{
			emit connectionEstablished();
		}
		else
		{
			emit connectionFailed();
		}
	});
	connectionThread.detach();
}

void FlashEditWindow::connectionEstablishedAction()
{
	setState(State::CONNECTED);
}

void FlashEditWindow::connectionFailedAction()
{
	setState(State::DISCONNECTED);
	QMessageBox::critical(this, "Error", "Failed to connect.");
}

void FlashEditWindow::readAction()
{
	setState(State::READING);

	std::thread readThread([this]()
	{
		bool success = flashProgrammer.readAll(data);

		flashProgrammer.saveTxtAndBinFiles("memory", data);
		flashProgrammer.saveAnalysisFile("memory.analysis.txt", data);

		if (success)
		{
			emit readSucceeded();
		}
		else
		{
			emit readFailed();
		}
	});
	readThread.detach();
}

void FlashEditWindow::updatePageFields()
{
	const uint32_t* arr = (const uint32_t*) &data[FE_PAGE_LINES_AND_SHOTS_SECONDARY * FE_PAGE_SIZE];

	ui->pageXValASB->setValue(arr[0]);
	ui->pageXValBSB->setValue(arr[1]);
	ui->pageXValCSB->setValue(arr[2]);
	ui->pageXValDSB->setValue(data[FE_PAGE_LINES_AND_SHOTS_SECONDARY * FE_PAGE_SIZE + 3 * sizeof(uint32_t)]);

	arr = (const uint32_t*) &data[FE_PAGE_LINES_AND_SHOTS_PRIMARY * FE_PAGE_SIZE];

	ui->pageYValASB->setValue(arr[0]);
	ui->pageYValBSB->setValue(arr[1]);
	ui->pageYValCSB->setValue(arr[2]);
	ui->pageYValDSB->setValue(data[FE_PAGE_LINES_AND_SHOTS_PRIMARY * FE_PAGE_SIZE + 3 * sizeof(uint32_t)]);

	arr = (const uint32_t*) &data[FE_PAGE_SERIAL_NO_AND_LIMIT * FE_PAGE_SIZE + 1 * sizeof(uint8_t)];

	ui->pageZValESB->setValue(data[FE_PAGE_SERIAL_NO_AND_LIMIT * FE_PAGE_SIZE]);
	ui->pageZValFSB->setValue(arr[0]);
	ui->pageZValGSB->setValue(arr[1]);
}

void FlashEditWindow::updateDataFromPageFields()
{
	uint32_t* arr = (uint32_t*) &data[FE_PAGE_LINES_AND_SHOTS_SECONDARY * FE_PAGE_SIZE];

	arr[0] = (uint32_t) ui->pageXValASB->value();
	arr[1] = (uint32_t) ui->pageXValBSB->value();
	arr[2] = (uint32_t) ui->pageXValCSB->value();
	data[FE_PAGE_LINES_AND_SHOTS_SECONDARY * FE_PAGE_SIZE + 3 * sizeof(uint32_t)] = (uint8_t) ui->pageXValDSB->value();

	arr = (uint32_t*) &data[FE_PAGE_LINES_AND_SHOTS_PRIMARY * FE_PAGE_SIZE];

	arr[0] = (uint32_t) ui->pageYValASB->value();
	arr[1] = (uint32_t) ui->pageYValBSB->value();
	arr[2] = (uint32_t) ui->pageYValCSB->value();
	data[FE_PAGE_LINES_AND_SHOTS_PRIMARY * FE_PAGE_SIZE + 3 * sizeof(uint32_t)] = (uint8_t) ui->pageYValDSB->value();

	arr = (uint32_t*) &data[FE_PAGE_SERIAL_NO_AND_LIMIT * FE_PAGE_SIZE + 1 * sizeof(uint8_t)];

	data[FE_PAGE_SERIAL_NO_AND_LIMIT * FE_PAGE_SIZE] = (uint8_t) ui->pageZValESB->value();
	arr[0] = (uint32_t) ui->pageZValFSB->value();
	arr[1] = (uint32_t) ui->pageZValGSB->value();
}

void FlashEditWindow::readSucceededAction()
{
	setState(State::READ_DONE);

	if (data.size() != FE_TOTAL_MEM_SIZE)
	{
		QMessageBox::critical(this, "Error", "Incorrect data size.: " + QString::number(data.size()));
		return;
	}
	updatePageFields();
}

void FlashEditWindow::readFailedAction()
{
	setState(State::CONNECTED);
	QMessageBox::critical(this, "Error", "Failed to read memory.");
}

void FlashEditWindow::writeAction()
{
	if (data.size() != FE_TOTAL_MEM_SIZE)
	{
		QMessageBox::critical(this, "Error", "Incorrect data size.: " + QString::number(data.size()));
		return;
	}
	setState(State::WRITING);

	updateDataFromPageFields();

	std::thread writeThread([this]()
	{
		if (flashProgrammer.writeAll(data))
		{
			emit writeSucceeded();
		}
		else
		{
			emit writeFailed();
		}
	});
	writeThread.detach();
}

void FlashEditWindow::writeSucceededAction()
{
	setState(State::WRITE_DONE);
}

void FlashEditWindow::writeFailedAction()
{
	setState(State::READ_DONE);
	QMessageBox::critical(this, "Error", "Failed to write memory.");
}

} /* namespace fe */
