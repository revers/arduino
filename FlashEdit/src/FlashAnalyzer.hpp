#ifndef FLASHANALYZER_HPP_
#define FLASHANALYZER_HPP_

#include <ostream>
#include <vector>

namespace fe
{

namespace FlashAnalyzer
{

bool analyze(const std::vector<uint8_t>& data, std::ostream& out);

} /* namespace FlashAnalyzer */

} /* namespace fe */

#endif /* FLASHANALYZER_HPP_ */
